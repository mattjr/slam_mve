// Test cases for perspective 3 point algorithms.
// Written by Simon Fuhrmann.

#include <gtest/gtest.h>

#include <vector>

#include "math/vector.h"
#include "math/matrix.h"
#include "math/quaternion.h"
#include "sfm/camera_pose.h"
#include "sfm/fundamental.h"
#include "sfm/ransac.h"
#include "sfm/ransac_fundamental.h"
#include "sfm/triangulate.h"
#include "stereo/stereo_features.h"
#include "stereo/ransac_stereo_pose_rigid.h"
#include "stereo/pose_stereo_rigid.h"
#include "test_with_data_settings.h"
#include "util/string.h"
#include "util/file_system.h"
#include "util/tokenizer.h"
#include "stereo/stereo_filename_utils.h"
#include "mve/image.h"
#include "mve/image_io.h"
#include "sfm/nearest_neighbor.h"
#include "sfm/feature_set.h"
#include "sfm/bundler_common.h"
#include "sfm/bundler_features.h"
#include "sfm/bundler_matching.h"
#include "sfm/bundler_tracks.h"
#include "sfm/bundler_init_pair.h"
#include "sfm/bundler_intrinsics.h"
#include "sfm/bundler_incremental.h"
#include "stereo/stereo_features.h"
#include "stereo/stereo_matching.h"
#include "stereo/stereo_incremental.h"
#include "stereo/stereo_init_pair.h"
using namespace std;
namespace
{
math::Matrix4d load_gt_pose(std::string filename){
    fstream fin(filename.c_str());
       string line;
       int row=0;
       math::Matrix4d trans;
       while(getline(fin, line))
       {
           //the following line trims white space from the beginning of the string
           line.erase(line.begin(), find_if(line.begin(), line.end(), not1(ptr_fun<int, int>(isspace))));

           if(line[0] == '#') continue;

           if(stringstream(line) >> trans(row,0) >> trans(row,1) >> trans(row,2) >>trans(row,3))
            row++;
           else{
              std::cerr << "Failed to load GT trans correctly test will fail\n";
               exit(-1);
           }
       }
       return trans;
}

void
fill_ground_truth_pose (sfm::CameraPose* pose1_master,sfm::CameraPose* pose1_slave, sfm::CameraPose* pose2_master, sfm::CameraPose* pose2_slave,double tx)
{
    // Calibration with focal lenght 1 and 800x600 image.
    // The first camera looks straight along the z axis.
    if (pose1_master != NULL)
    {
        pose1_master->set_k_matrix(800, 800 / 2, 600 / 2);
        math::matrix_set_identity(*pose1_master->R, 3);
        pose1_master->t.fill(0.0);
        *pose1_slave=*pose1_master;
        pose1_slave->t[0]+=tx;

    }

    // The second camera is at (1,0.25,0) and rotated 45deg to the left.
    if (pose2_master != NULL)
    {
        pose2_master->set_k_matrix(800, 800 / 2, 600 / 2);
        pose2_master->R.fill(0.0);
        double const angle = MATH_PI / 4;
        pose2_master->R(0,0) = std::cos(angle); pose2_master->R(0,2) = std::sin(angle);
        pose2_master->R(1,1) = 1.0;
        pose2_master->R(2,0) = -std::sin(angle); pose2_master->R(2,2) = std::cos(angle);
        pose2_master->t.fill(0.0); pose2_master->t[0] = 1.0;pose2_master->t[1] = 0.25;
        //pose2_master->t = pose2_master->R * -pose2_master->t;
        *pose2_slave=*pose2_master;
        pose2_slave->t[0]+=tx;

    }
}
void
fill_eight_random_points (std::vector<math::Vec3d>* points)
{
    points->push_back(math::Vec3d(-0.31, -0.42, 1.41));
    points->push_back(math::Vec3d( 0.04,  0.01, 0.82));
    points->push_back(math::Vec3d(-0.25, -0.24, 1.25));
    points->push_back(math::Vec3d( 0.47,  0.22, 0.66));
    points->push_back(math::Vec3d( 0.13,  0.03, 0.89));
    points->push_back(math::Vec3d(-0.13, -0.46, 1.15));
    points->push_back(math::Vec3d( 0.21, -0.23, 1.33));
    points->push_back(math::Vec3d(-0.42,  0.38, 0.62));
}
std::string
remove_file_extension (std::string const& filename)
{
    std::size_t pos = filename.find_last_of('.');
    if (pos != std::string::npos)
        return filename.substr(0, pos);
    return filename;
}
}
TEST(StereoRelPose, SyntheticPoseTest1)
{
    char fname[1024];
    std::map<std::pair<int,int>,math::Matrix4d> gt_mats;
    int num_frames=100;
    for(int i=0; i < num_frames; i++){
        sprintf(fname,"%s/from_%03d_to_%03d.txt",TEST_DATA_DIR,i,i+1);
        gt_mats.insert(make_pair(make_pair(i,i+1),load_gt_pose(fname)));
    }
    std::vector<math::Matrix4d> poses;
    std::vector<math::Matrix4d> calc_poses;

    for(auto const& trans: gt_mats){
        cout <<trans.first.first << "-" <<trans.first.second<<": " << trans.second<<endl;
    }

    for(auto const& trans: gt_mats){
        if(poses.empty())
            poses.push_back(trans.second);
        else
            poses.push_back(trans.second*poses.back());

    }
    util::fs::Directory dir;
    try { dir.scan(TEST_DATA_DIR); }
    catch (std::exception& e)
    {
        std::cerr << "Error scanning input dir: " << e.what() << std::endl;
        std::exit(EXIT_FAILURE);
    }
    std::sort(dir.begin(), dir.end());
    util::fs::Directory master_dir,slave_dir;
    ExpandWildCard(dir,master_dir,"c0");
    ExpandWildCard(dir,slave_dir,"c1");
    std::vector<std::pair<util::fs::File,util::fs::File> > pairs;
    RemoveNonPairs(master_dir,slave_dir,"c0","c1",0,pairs);
    int id_cnt = 1;
    unsigned int loaded_ok=0;
     mve::Scene::Ptr scene= mve::Scene::create();
    for (auto p: pairs)
    {

        if (p.first.is_dir || p.second.is_dir)
        {
            std::cout << "Skipping directory " << p.first.name << std::endl;
            continue;
        }

        std::string fname_master = p.first.name;
        std::string afname_master = p.first.get_absolute_name();

//        std::cout << "Importing image " << fname_master << "..." << std::endl;
        mve::ImageBase::Ptr image_master = mve::image::load_file(afname_master);
        if (image_master == NULL)
            continue;


        std::string fname_slave = p.second.name;
        std::string afname_slave = p.second.get_absolute_name();

        mve::ImageBase::Ptr image_slave = mve::image::load_file(afname_slave);
        if (image_slave == NULL)
            continue;
        /* Create view, set headers, add image. */
        mve::View::Ptr view = mve::View::create();
        view->set_id(id_cnt);
        view->set_name(remove_file_extension(fname_master));
        view->set_image_ref(afname_master, "original");
        view->set_image_ref(afname_slave, "slave");
        std::vector<char> calib_data;
        string stereo_calib;
        stereo_calib=string(TEST_DATA_DIR)+"/stereo.calib";
        ReadAllBytes(stereo_calib.c_str(),calib_data);
        if(calib_data.size() == 0){
            std::cout <<"Can't load stereo calib file:" <<stereo_calib << std::endl;
            throw std::invalid_argument("Calib file invaid");
        }

        mve::ByteImage::Ptr calib_data_image = mve::ByteImage::create(calib_data.size(), 1, 1);
        std::copy(calib_data.begin(), calib_data.end(), calib_data_image->begin());
        view->set_blob(calib_data_image,  "stereocalib");
        scene->get_views().push_back(view);

        id_cnt += 1;
        loaded_ok += 1;

    }
    EXPECT_TRUE(loaded_ok == pairs.size());

    sfm::bundler::stereo::StereoViewportList viewports;
    sfm::bundler::stereo::StereoPairwiseMatching pairwise_matching;


    sfm::bundler::stereo::StereoFeatures::Options feature_opts;
    //feature_opts.image_embedding = conf.original_name;
   // feature_opts.max_image_size = conf.max_image_size;
    feature_opts.feature_options.feature_types = sfm::FeatureSetExtended::FEATURE_ALL_EXTENDED;


    {
        sfm::bundler::stereo::StereoFeatures bundler_features(feature_opts);
        bundler_features.compute(scene, &viewports);

    }
    /* Exhaustive matching between all pairs of views. */
    sfm::bundler::stereo::StereoMatching::Options matching_opts;
    //matching_opts.ransac_opts.max_iterations = 1000;
    //matching_opts.ransac_opts.threshold = 0.0015;
    matching_opts.ransac_opts.verbose_output = true;
    //matching_opts.use_lowres_matching = true;
    matching_opts.match_num_previous_frames = 1;//conf.video_matching;
    {

        sfm::bundler::stereo::StereoMatching bundler_matching(matching_opts);
        bundler_matching.compute(viewports, &pairwise_matching);
    }
    int error_count=0;
    for(auto tvm: pairwise_matching.tvms){
        math::Matrix<double, 4, 4> calc=tvm.relpose.vstack(math::Vec4d(0,0,0,1.0));
        if(gt_mats.count(make_pair(tvm.view_2_id,tvm.view_1_id))){
            if(calc_poses.empty())
                calc_poses.push_back(calc);
            else
                calc_poses.push_back(calc*calc_poses.back());

            math::Matrix<double, 4, 4> gt=gt_mats[make_pair(tvm.view_2_id,tvm.view_1_id)];
                bool ok=calc.is_similar(gt,1e-1);
                EXPECT_TRUE(ok);

                if(!ok){
                    error_count++;
                    cout << "Matrix not the same GT:\n";
                    cout << gt <<endl << "Calc\n";
                    cout <<calc <<endl;

                }
        }

    }
    for(unsigned int i = 0; i < calc_poses.size(); i++){
        bool ok=calc_poses[i].is_similar(poses[i],1e-1);

        if(!ok){
            error_count++;
            cout << "Matrix not the same GT:\n";
            cout << poses[i] <<endl << "Calc\n";
            cout <<calc_poses[i] <<endl;

        }
    }

    printf("Error Count %d/%d\n",error_count,(int)pairwise_matching.tvms.size());

}
