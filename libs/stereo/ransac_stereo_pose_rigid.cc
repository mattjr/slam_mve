/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <set>
#include <iostream>

#include "util/system.h"
#include "math/matrix_tools.h"
#include "sfm/ransac_pose_p3p.h"
#include "stereo/pose_stereo_rigid.h"
#include "stereo/ransac_stereo_pose_rigid.h"

SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
static int gcnt=0;
RansacPoseRigidStereo::RansacPoseRigidStereo (Options const& options)
    : RansacPoseP3P(options),opts(options)
{
}

void
RansacPoseRigidStereo::estimate (StereoMatchCorrespondences2D3D const& corresp,
    math::Matrix<double, 3, 3> const& k_matrix, double tx, Result* result)
{
    if (this->opts.verbose_output)
    {
        std::cout << "RANSAC-3: Running for " << this->opts.max_iterations
            << " iterations, threshold " << this->opts.threshold
            << "..." << std::endl;
    }

    std::cout << corresp.size() << std::endl;
    std::vector<int> inliers;
    inliers.reserve(corresp.size());
    for (int iteration = 0; iteration < this->opts.max_iterations; ++iteration)
    {
        /* Compute [R|t] */
        Pose pose;
        this->compute_rigid(corresp, pose);
        //std::cout << "putative pose " <<pose << std::endl;
        /* Check all putative solutions and count inliers. */
        bool found_better_solution = false;

        this->find_inliers(corresp,k_matrix,tx, pose, &inliers);
        if (inliers.size() > result->inliers.size())
        {
            result->pose = pose;
            std::swap(result->inliers, inliers);
            inliers.reserve(corresp.size());
            found_better_solution = true;
        }


        if (found_better_solution && this->opts.verbose_output)
        {
            std::cout << "RANSAC-3: Iteration " << iteration
                << ", inliers " << result->inliers.size() << " ("
                << (100.0 * result->inliers.size() / corresp.size())
                << "%)" << std::endl;
        }
    }   
}

void
RansacPoseRigidStereo::compute_rigid (StereoMatchCorrespondences2D3D const& corresp,
   Pose &pose)
{
    if (corresp.size() < 3)
        throw std::invalid_argument("At least 3 correspondences required");

    /* Draw 3 unique random numbers. */
    std::set<int> result;
    while (result.size() < 3)
        result.insert(util::system::rand_int() % corresp.size());

    std::set<int>::const_iterator iter = result.begin();
    StereoMatchCorrespondence2D3D const& c1(corresp[*iter++]);
    StereoMatchCorrespondence2D3D const& c2(corresp[*iter++]);
    StereoMatchCorrespondence2D3D const& c3(corresp[*iter]);

    ::stereo::pose_stereo_rigid(
                math::Vec3d(c1.sc1.p3d), math::Vec3d(c2.sc1.p3d), math::Vec3d(c3.sc1.p3d),
                math::Vec3d(c1.sc2.p3d), math::Vec3d(c2.sc2.p3d), math::Vec3d(c3.sc2.p3d),
                pose);
}

void
RansacPoseRigidStereo::find_inliers (StereoMatchCorrespondences2D3D const& corresp,
    math::Matrix<double, 3, 3> const& k_matrix, double tx,
             Pose const& pose, std::vector<int>* inliers)
{
    inliers->resize(0);
    double const square_threshold = MATH_POW2(this->opts.rigid_threshold);
    for (std::size_t i = 0; i < corresp.size(); ++i)
    {
      //  std::cout <<"see "<<pose<<"\n";
        StereoMatchCorrespondence2D3D const& c = corresp[i];
        math::Vec4d p3d(c.sc1.p3d[0], c.sc1.p3d[1], c.sc1.p3d[2], 1.0);
        //std::cout << "RANSAC Orig: " <<p3d << std::endl;

        math::Vec3d xyd = cam_pt_to_rect_xyd(k_matrix,tx, pose * p3d);
        math::Vec3d pos2d_d(c.sc2.p2d[0],c.sc2.p2d[1],c.sc2.p2d_slave[0]-c.sc2.p2d[0]);
       // std::cout << "RANSAC reproj: " <<p2d << std::endl;

        //std::cout << "Projected " << p2d<< " sc2 view: " << c.sc2.p2d[0] << " "<< c.sc2.p2d[1] << " ";
        //std::cout << "sc1 view : " << c.sc1.p2d[0] << " "<< c.sc1.p2d[1] << "\n";
       // std::cout << "Disp compare calc: "<< (p2d[2]) << " stored: " <<(c.sc2.p2d[0]-c.sc2.p2d_slave[0]) << std::endl;
//std::cout << p2d[0] << " "<< p2d[1]  << "-- "<< c.sc2.p2d[0] << " "<<c.sc2.p2d[1] <<std::endl;
        double square_error = (pos2d_d - xyd).square_norm();/*MATH_POW2(p2d[0] - c.sc2.p2d[0])
            + MATH_POW2(p2d[1] - c.sc2.p2d[1]) +
               MATH_POW2(p2d[2] - (c.sc2.p2d[0]-c.sc2.p2d_slave[0])) ;*/
       // printf("x1: x %f y %f, sx: %f sy %f\n", c.sc1.p2d[0],c.sc1.p2d[1], c.sc1.p2d_slave[0],c.sc1.p2d_slave[1]) ;
        //printf("pred: %f %f %f \n", p2d[0] , p2d[1],p2d[2] ) ;

    //  printf("x2: %f -- %f %f, y: %f %f, d: %f %f\n", square_error,xyd[0] , c.sc2.p2d[0],xyd[1],c.sc2.p2d[1],xyd[2], (c.sc2.p2d[0]-c.sc2.p2d_slave[0])) ;
      //  std::cout <<this->opts.threshold<< " "<<square_error <<  " " <<(c.sc2.p2d_slave[0]-c.sc2.p2d[0]) << " "<<p2d[2]<<"\n";
        if (square_error < square_threshold)
            inliers->push_back(i);
    }
}
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END
