/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include <iostream>
#include <fstream>
#include <cstring>
#include <cerrno>

#include "util/exception.h"
#include "util/timer.h"
#include "sfm/sift.h"
#include "sfm/ransac.h"
#include "sfm/bundler_matching.h"
#include "stereo/stereo_matching.h"
#include "stereo/stereo_common.h"
#include "stereo/stereo_features.h"
#include "stereo/plotting_utils.h"
#include "stereo/image_features.hpp"
#include <opencv/highgui.h>
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN

void
StereoMatching::compute (StereoViewportList const& viewports,
    StereoPairwiseMatching* pairwise_matching)
{
    std::size_t num_pairs = viewports.size() * (viewports.size() - 1) / 2;
    std::size_t num_done = 0;

    if (this->progress != nullptr)
    {
        this->progress->num_total = num_pairs;
        this->progress->num_done = 0;
    }

    #pragma omp parallel for schedule(dynamic)
    for (std::size_t i = 0; i < num_pairs; ++i)
    {
#pragma omp critical
        {
            num_done += 1;
            if (this->progress != nullptr)
                this->progress->num_done += 1;

            float percent = (num_done * 1000 / num_pairs) / 10.0f;
            std::cout << "\rMatching pair " << num_done << " of "
                << num_pairs << " (" << percent << "%)..." << std::flush;
        }

        int const view_1_id = (int)(0.5 + std::sqrt(0.25 + 2.0 * i));
        int const view_2_id = (int)i - view_1_id * (view_1_id - 1) / 2;
        if (this->opts.match_num_previous_frames != 0
            && view_2_id + this->opts.match_num_previous_frames < view_1_id)
            continue;

        FeatureSetExtended const& view_1 = viewports[view_1_id].features;
        FeatureSetExtended const& view_2 = viewports[view_2_id].features;
        if (view_1.positions.empty() || view_2.positions.empty())
            continue;

        /* Match the views. */
        util::WallTimer timer;
        std::stringstream message;
        CorrespondenceIndices matches;
        math::Matrix<double, 3, 4> relpose;
        this->two_view_matching(viewports[view_1_id],viewports[view_2_id],view_1, view_2, &matches,relpose, message);
       // std::cout << view_1_id <<"-" <<view_2_id<< relpose<<"\n";
        std::size_t matching_time = timer.get_elapsed();

        if (matches.empty())
        {
#pragma omp critical
            std::cout << "\rPair (" << view_1_id << ","
                << view_2_id << ") rejected, "
                << message.str() << std::endl;
            continue;
        }

        /* Successful two view matching. Add the pair. */
        StereoTwoViewMatching matching;
        matching.view_1_id = view_1_id;
        matching.view_2_id = view_2_id;
        matching.relpose = relpose;
        std::swap(matching.matches, matches);

#pragma omp critical
        {
            pairwise_matching->view_ids2_tvms_idx[std::make_pair(view_1_id,view_2_id)]=pairwise_matching->tvms.size();
            pairwise_matching->tvms.push_back(matching);
            std::cout << "\rPair (" << view_1_id << ","
                << view_2_id << ") matched, " << matching.matches.size()
                << " inliers, took " << matching_time << " ms." << std::endl;
        }
    }

    std::cout << "\rFound a total of " << pairwise_matching->tvms.size()
        << " matching image pairs." << std::endl;
}

void
StereoMatching::two_view_matching (StereoViewport const&viewport_1, StereoViewport const&viewport_2,FeatureSetExtended const& view_1,
    FeatureSetExtended const& view_2, CorrespondenceIndices* matches,math::Matrix<double, 3, 4> &relpose,
    std::stringstream& message)
{
    /* Low-res matching if number of features is large. */
   /* if (this->opts.use_lowres_matching
        && view_1.positions.size() * view_2.positions.size() > 1000000)
    {
        int const num_matches = view_1.match_lowres(view_2,
            this->opts.num_lowres_features);
        if (num_matches < this->opts.min_lowres_matches)
        {
            message << "only " << num_matches
                << " of " << this->opts.min_lowres_matches
                << " low-res matches.";
            return;
        }
    }
*/
    /* Perform two-view descriptor matching. */
    sfm::Matching::Result matching_result;
    view_1.match(view_2, &matching_result,viewport_1.stereo_calibration,true,&viewport_1.matches_master_slave,&viewport_2.matches_master_slave);
    int num_matches = sfm::Matching::count_consistent_matches(matching_result);


    /*    // std::cout << "AAAAAA Num Matches "<<num_matches <<std::endl;
    cv::Mat outimg;
    plotting_utils::drawPair2PairMatches(outimg,viewport_1.image,viewport_1.image_slave,
                                         viewport_2.image,viewport_2.image_slave,viewport_1,viewport_2,matching_result,viewport_1.stereo_calibration
                                         ,true);
   cv::namedWindow("A");
    cv::Mat small;
    cv::resize(outimg,small,cv::Size(),0.5,0.5);
    cv::imshow("A",small);
    cv::waitKey(0);

    */

    /* Require at least 3 matches. Check threshold. */
    int const min_matches_thres = std::max(3, this->opts.min_feature_matches);
    if (num_matches < min_matches_thres)
    {
        message << "matches below threshold of "
            << min_matches_thres << ".";
        return;
    }

    two_view_matching_internal( viewport_1, viewport_2,  matches,relpose,
        message,matching_result);
}
    void
    StereoMatching::two_view_matching_internal(StereoViewport const&viewport_1, StereoViewport const&viewport_2, CorrespondenceIndices* matches,math::Matrix<double, 3, 4> &relpose,
        std::stringstream& message,sfm::Matching::Result &matching_result){
        int const min_matches_thres = std::max(3, this->opts.min_feature_matches);

    /* Build correspondences from feature matching result. */
    StereoMatchCorrespondences2D3D unfiltered_matches;
    sfm::CorrespondenceIndices unfiltered_indices;
    int vp1_3d_invalid=0;
    int vp2_3d_invalid=0;
    int general_invalid=0;

    {

        std::vector<int> m12;
        for(size_t i=0; i < matching_result.matches_1_2.size()-viewport_1.features.orb_kp.size(); i++){
            m12.push_back( matching_result.matches_1_2[i]);
        }




        for (std::size_t i = 0; i < m12.size(); ++i)
        {
            if (m12[i] < 0 || viewport_1.pts_3d[i][0] == -999 || viewport_2.pts_3d[m12[i]][0] == -999){
             if(viewport_1.pts_3d[i][0] == -999)
    vp1_3d_invalid++;
                 if(viewport_2.pts_3d[m12[i]][0] == -999)
                vp2_3d_invalid++;

                 general_invalid++;
                 continue;
            }

            StereoMatchCorrespondence2D3D match;
            match.sc1.p3d[0]=viewport_1.pts_3d[i][0];
            match.sc1.p3d[1]=viewport_1.pts_3d[i][1];
            match.sc1.p3d[2]=viewport_1.pts_3d[i][2];

            match.sc1.p2d_slave[0] = viewport_1.features_slave.positions[viewport_1.matches_master_slave[i]][0];
            match.sc1.p2d_slave[1] = viewport_1.features_slave.positions[viewport_1.matches_master_slave[i]][1];

            match.sc1.p2d[0] = viewport_1.features.positions[i][0];
            match.sc1.p2d[1] = viewport_1.features.positions[i][1];
          //  match.sc1.p1[0] = view_1.positions[i][0];
          //  match.sc1.p1[1] = view_1.positions[i][1];

            match.sc2.p3d[0]=viewport_2.pts_3d[m12[i]][0];
            match.sc2.p3d[1]=viewport_2.pts_3d[m12[i]][1];
            match.sc2.p3d[2]=viewport_2.pts_3d[m12[i]][2];

            match.sc2.p2d[0] = viewport_2.features.positions[m12[i]][0];
            match.sc2.p2d[1] = viewport_2.features.positions[m12[i]][1];

            match.sc2.p2d_slave[0] = viewport_2.features_slave.positions[viewport_2.matches_master_slave[m12[i]]][0];
            match.sc2.p2d_slave[1] = viewport_2.features_slave.positions[viewport_2.matches_master_slave[m12[i]]][1];

            unfiltered_matches.push_back(match);
            unfiltered_indices.push_back(std::make_pair(i, m12[i]));
        }
    }
    /* Require at least 3 matches. Check threshold. */
    if ((int)unfiltered_matches.size() < min_matches_thres)
    {
        message << "matches below threshold of "
            << min_matches_thres << ".";
        return;
    }
    /* Initialize a temporary camera. */
    CameraPose temp_camera;
    temp_camera.K=viewport_1.stereo_calibration.leftCamRect->k_matrix; //set_k_matrix(viewport_1.focal_length, 0.0, 0.0);
    double tx=viewport_1.stereo_calibration.T_mat(0);

    /* Compute pose from 2D-3D correspondences using P3P. */
    RansacPoseRigidStereo::Result ransac_result;
    {
        RansacPoseRigidStereo ransac(this->opts.pose_rigid_opts);
        ransac.estimate(unfiltered_matches, temp_camera.K,tx, &ransac_result);
    }
    std::cout << "Inliers : "<<ransac_result.inliers.size() << " of size:" << unfiltered_matches    .size()<< " "<< general_invalid<< " :"<<vp1_3d_invalid<< " " << vp2_3d_invalid<<"\n";
    /* Cancel if inliers are below a 20% threshold. */
    if (5 * ransac_result.inliers.size() < unfiltered_matches.size())
    {
        if (this->opts.verbose_output)
            std::cout << "Only " << ransac_result.inliers.size()
                << " 2D-3D correspondences inliers ("
                << (100 * ransac_result.inliers.size() / unfiltered_matches.size())
                << "%). Skipping view." << std::endl;
        return;
    }


    int num_inliers=ransac_result.inliers.size();
    /* Create Two-View matching result. */
    matches->clear();
    matches->reserve(num_inliers);
    for (int i = 0; i < num_inliers; ++i)
    {
        int const inlier_id = ransac_result.inliers[i];
        matches->push_back(unfiltered_indices[inlier_id]);
       /* math::Vec4d p3d(unfiltered_matches[inlier_id].sc1.p3d[0], unfiltered_matches[inlier_id].sc1.p3d[1], unfiltered_matches[inlier_id].sc1.p3d[2], 1.0);
        math::Vec3d xyd = cam_pt_to_rect_xyd(temp_camera.K,tx, ransac_result.pose * p3d);
        math::Vec3d pos2d_d(unfiltered_matches[inlier_id].sc2.p2d[0],unfiltered_matches[inlier_id].sc2.p2d[1],unfiltered_matches[inlier_id].sc2.p2d_slave[0]-unfiltered_matches[inlier_id].sc2.p2d[0]);


        double square_error = (pos2d_d - xyd).square_norm();
        std::cout << "E: " << square_error  << " "<<p3d<< " " << ransac_result.pose<<std::endl;*/
    }

    StereoMatchCorrespondences2D3D densify_matches;
    sfm::CorrespondenceIndices densify_indices;
    int orb_cnt=0;
    int valid=0;
    cv::Mat wrapped_img(viewport_1.image->height(), viewport_1.image->width(),
                    viewport_1.image->channels() > 1 ? CV_8UC3:CV_8U);
    ImageFeatures img_f(wrapped_img,viewport_2.features.orb_kp,viewport_2.features.orb_desc,10);


    std::vector< cv::Point2f > preds (viewport_1.features.orb_kp.size());

  for(size_t i=viewport_1.pts_3d.size()-viewport_1.features.orb_kp.size(); i < viewport_1.pts_3d.size(); i++,orb_cnt++){

        if ( viewport_1.pts_3d[i][0] == -999){
            continue;
        }

        math::Vec4d p3d(viewport_1.pts_3d[i][0], viewport_1.pts_3d[i][1], viewport_1.pts_3d[i][2], 1.0);
        math::Vec3d xyd = cam_pt_to_rect_xyd(temp_camera.K,tx, ransac_result.pose * p3d);
        cv::Point2f prediction( xyd[0],xyd[1]);
        const cv::Mat &desc=viewport_1.features.orb_desc.row(orb_cnt);
        int offset =viewport_2.pts_3d.size()-viewport_2.features.orb_kp.size();
        int match=img_f.FindMatch(prediction,desc,viewport_1.features.orb_bf_matcher,64,0) ;
        if(match != -1){
            valid++;
            matches->push_back(std::make_pair(i,offset+match));

        }
        preds[orb_cnt]=prediction;
    }

  sfm::Matching::Result valid_matching_result;
  valid_matching_result.matches_1_2.resize(matching_result.matches_1_2.size(),-1);

  valid_matching_result.matches_2_1.resize(matching_result.matches_2_1.size(),-1);

  for(int i=0; i < matches->size(); i++){
      valid_matching_result.matches_1_2[matches->at(i).first]=matches->at(i).second;
      valid_matching_result.matches_2_1[matches->at(i).second]=matches->at(i).first;

  }
  /*
  cv::Mat outimg;
      plotting_utils::drawPair2PairMatches(outimg,viewport_1.image,viewport_1.image_slave,
                                           viewport_2.image,viewport_2.image_slave,viewport_1,viewport_2,valid_matching_result,viewport_1.stereo_calibration
                                           ,true);
     cv::namedWindow("A");
      cv::Mat small;
      cv::resize(outimg,small,cv::Size(),0.5,0.5);
      cv::imshow("A",small);
      cv::waitKey(0);
  */
  std::cout << "Asssert " << orb_cnt << " "<<preds.size();

  //  viewport_1.features.orb_kp;
    //        viewport_1.features_slave.orb_kp
    std::cout  <<std::endl<<std::endl<<"Densify " << valid<<std::endl<<std::endl;
    relpose = ransac_result.pose;
    //math::Matrix<double, 4, 4>   world2cam = relpose.vstack(math::Vec4d(0,0,0,1.0));
    //std::cout << "IN RANSAC \n" << world2cam<<std::endl;


}
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END
