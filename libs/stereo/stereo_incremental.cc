/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the GPL 3 license. See the LICENSE.txt file for details.
 */

#include <limits>
#include <iostream>
#include <utility>
#include <array>

#include "util/timer.h"
#include "math/matrix_tools.h"
#include "sfm/triangulate.h"
#include "sfm/pba_cpu.h"
#include "stereo/stereo_incremental.h"
#include "stereo/stereo_common.h"
#include "stereo/defines.h"

#include "stereo/costfunc.h"
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
static int gc=0;
void
Incremental::initialize (StereoViewportList* viewports, TrackList* tracks,
                         StereoPairwiseMatching* pairwise_matching)
{
    this->viewports = viewports;
    this->tracks = tracks;
    this->pairwise_matching = pairwise_matching;

    if (this->viewports->empty())
        throw std::invalid_argument("No viewports given");

    /* Check if at least two cameras are initialized. */
    std::size_t num_valid_cameras = 0;
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
        if (this->viewports->at(i).pose.is_valid())
            num_valid_cameras += 1;
    if (num_valid_cameras < 2)
        throw std::invalid_argument("Two or more valid cameras required");

    /* Set track positions to invalid state. */
    for (std::size_t i = 0; i < tracks->size(); ++i)
    {
        Track& track = tracks->at(i);
        track.invalidate();
    }
}

/* ---------------------------------------------------------------- */

void
Incremental::find_next_views (std::vector<int>* next_views)
{
    /* Create mapping from valid tracks to view ID. */
    std::vector<std::pair<int, int> > valid_tracks(this->viewports->size());
    for (std::size_t i = 0; i < valid_tracks.size(); ++i)
        valid_tracks[i] = std::make_pair(0, static_cast<int>(i));

    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
     //   if (!track.is_valid())
       //     continue;

        for (std::size_t j = 0; j < track.features.size(); ++j)
        {
            int const view_id = track.features[j].view_id;
           // std::cout << "Track ID  " << view_id <<std::endl;
            if (this->viewports->at(view_id).is_added())
                continue;
            valid_tracks[view_id].first += 1;
        }
    }

    /* Sort descending by number of valid tracks. */
    std::sort(valid_tracks.rbegin(), valid_tracks.rend());

    /* Return unreconstructed views with most valid tracks. */
    next_views->clear();
    for (std::size_t i = 0; i < valid_tracks.size(); ++i)
    {
        if (valid_tracks[i].first > 2)
            next_views->push_back(valid_tracks[i].second);
    }
}

/* ---------------------------------------------------------------- */

bool
Incremental::reconstruct_next_view (int view_id)
{
   /* for ( auto p : this->pairwise_matching->view_ids2_tvms_idx){
        std::cout << p.first.first << " "<< p.first.second<<std::endl;
    }*/
    StereoViewport &viewport = this->viewports->at(view_id);
    for (std::size_t i = 0; i < viewport.track_ids.size(); ++i)
    {

        int const track_id = viewport.track_ids[i];
        if (track_id < 0 )//|| !this->tracks->at(track_id).is_valid())
            continue;
        FeatureReferenceList const& feature_views = this->tracks->at(track_id).features;
        for(std::size_t j=0; j < feature_views.size(); j++){

            if(this->viewports->at(feature_views[j].view_id).added){

                int relrpose_idx;
                bool flip=false;
                if(this->pairwise_matching->view_ids2_tvms_idx.count(std::make_pair(feature_views[j].view_id,view_id)) != 0){
                     relrpose_idx=this->pairwise_matching->view_ids2_tvms_idx[std::make_pair(feature_views[j].view_id,view_id)];
                     flip=true;

                }
                else if(this->pairwise_matching->view_ids2_tvms_idx.count(std::make_pair(view_id,feature_views[j].view_id)) != 0 ){
                    relrpose_idx=this->pairwise_matching->view_ids2_tvms_idx[std::make_pair(view_id,feature_views[j].view_id)];

                }else
                    continue;
                //std::cout << "!!!!!!!!!FLIP: "<< flip <<std::endl;

                math::Matrix<double, 3, 4> &relpose=this->pairwise_matching->tvms.at(relrpose_idx).relpose;
                math::Matrix<double, 4, 4> rel4;

                CameraPose& pose = this->viewports->at(feature_views[j].view_id).pose;
                math::Matrix4d orig = (pose.R.hstack(pose.t)).vstack(math::Vec4d(0,0,0,1.0));

                if(!flip){
                  rel4=math::matrix_invert_trans(relpose.vstack(math::Vec4d(0,0,0,1.0)));
                }else{
                    rel4=relpose.vstack(math::Vec4d(0,0,0,1.0));
                }
                math::Matrix4d combo = rel4*orig;

                viewport.pose.R = combo.delete_row(3).delete_col(3) ;
                viewport.pose.t = combo.delete_row(3).col(3) ;

                /*if (this->opts.verbose_output)
                {
                    std::cout << "Reconstructed camera "
                        << view_id << " with focal length "
                        << pose.get_focal_length() << std::endl;
                }*/
                viewport.set_added(true);
                return true;

            }

        }

    }
#if 0
    if (this->opts.verbose_output)
    {
        std::cout << "Collected " << corr.size()
            << " 2D-3D correspondences." << std::endl;
    }

    /* Initialize a temporary camera. */
    CameraPose temp_camera;
    temp_camera.set_k_matrix(viewport.focal_length, 0.0, 0.0);

    /* Compute pose from 2D-3D correspondences using P3P. */
    RansacPoseP3P::Result ransac_result;
    {
        RansacPoseP3P ransac(this->opts.pose_p3p_opts);
        ransac.estimate(corr, temp_camera.K, &ransac_result);
    }

    /* Cancel if inliers are below a 33% threshold. */
    if (3 * ransac_result.inliers.size() < corr.size())
    {
        if (this->opts.verbose_output)
            std::cout << "Only " << ransac_result.inliers.size()
                << " 2D-3D correspondences inliers ("
                << (100 * ransac_result.inliers.size() / corr.size())
                << "%). Skipping view." << std::endl;
        return false;
    }
    else if (this->opts.verbose_output)
    {
        std::cout << "Selected " << ransac_result.inliers.size()
            << " 2D-3D correspondences inliers ("
            << (100 * ransac_result.inliers.size() / corr.size())
            << "%)." << std::endl;
    }

    /*
     * Remove outliers from tracks and tracks from viewport.
     * TODO: Once single cam BA has been performed and parameters for this
     * camera are optimized, evaluate outlier tracks and try to restore them.
     */
    for (std::size_t i = 0; i < ransac_result.inliers.size(); ++i)
        track_ids[ransac_result.inliers[i]] = -1;
    for (std::size_t i = 0; i < track_ids.size(); ++i)
    {
        if (track_ids[i] < 0)
            continue;
        this->tracks->at(track_ids[i]).remove_view(view_id);
        this->viewports->at(view_id).track_ids[feature_ids[i]] = -1;
    }
    track_ids.clear();
    feature_ids.clear();
#endif
    /* Commit camera using known K and computed R and t. */
    {

    }

    return false;
}
void
print_statistics (Triangulate::Statistics const& stats, std::ostream& out)
{
    int const num_rejected = stats.num_large_error
        + stats.num_behind_camera
        + stats.num_too_small_angle;

    out << "Triangulate Check: " << stats.num_new_tracks
        << " new tracks, rejected " << num_rejected
        << " bad tracks." << std::endl;
    if (stats.num_large_error > 0)
        out << "  Rejected " << stats.num_large_error
            << " tracks with large error." << std::endl;
    if (stats.num_behind_camera > 0)
        out << "  Rejected " << stats.num_behind_camera
            << " tracks behind cameras." << std::endl;
    if (stats.num_too_small_angle > 0)
        out << "  Rejected " << stats.num_too_small_angle
            << " tracks with unstable angle." << std::endl;
}

/* ---------------------------------------------------------------- */

void
Incremental::triangulate_new_tracks (void)
{
    Triangulate::Statistics *stats = new Triangulate::Statistics;

    //FILE*fp=fopen("triangulate.txt","w");
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        /* Skip tracks that have already been triangulated. */
        Track const& track = this->tracks->at(i);
        if (track.is_valid())
            continue;

        /*
         * Triangulate the track using all cameras. There can be more than two
         * cameras if the track was rejected in previous triangulation attempts.
         */
        math::Vec3d track_pos;
        bool found=false;
       // for (int j = (int)track.features.size()-1; j >= 0;  --j)
            for (int j = 0; j <(int)track.features.size(); j++)

        {
            int const view_id = track.features[j].view_id;
            if (!this->viewports->at(view_id).is_added())
                continue;
            int const feature_id = track.features[j].feature_id;
            CameraPose const& pose = this->viewports->at(view_id).pose;
            math::Vec3f const &pos3d = this->viewports->at(view_id).pts_3d[feature_id];
            track_pos =  math::matrix_invert_trans(pose.R.hstack(pose.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3) * math::Vec4d (pos3d[0],pos3d[1],pos3d[2],1.0);
           // track_pos = pose.R.hstack(pose.t) * math::Vec4d (pos3d[0],pos3d[1],pos3d[2],1.0); //(pose.R.transposed() * (pos3d - pose.t));
            found = true;
            if (stats != nullptr)
                stats->num_new_tracks += 1;
            break;
        }

        /* Accept track if triangulation was successful. */
        if (found){
            this->tracks->at(i).pos = track_pos;
        }else {
           this->tracks->at(i).pos=math::Vec3f(std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN(),std::numeric_limits<float>::quiet_NaN());
         }
    }
   // print_statistics(*stats,std::cout);
    delete stats;
    //fclose(fp);


}

/* ---------------------------------------------------------------- */

void
Incremental::bundle_adjustment_full (void)
{
    this->bundle_adjustment_intern(-1);
}

/* ---------------------------------------------------------------- */

void
Incremental::bundle_adjustment_single_cam (int view_id)
{
    this->bundle_adjustment_intern(view_id);
}

/* ---------------------------------------------------------------- */

//#define PBA_DISTORTION_TYPE pba::PROJECTION_DISTORTION
//#define PBA_DISTORTION_TYPE pba::MEASUREMENT_DISTORTION
#define PBA_DISTORTION_TYPE pba::NO_DISTORTION

void
Incremental::bundle_adjustment_intern_pba (int single_camera_ba)
{
    /* Configure PBA. */
    pba::SparseBundleCPU pba;
    pba.EnableRadialDistortion(PBA_DISTORTION_TYPE);
    pba.SetNextTimeBudget(0);
    if (single_camera_ba >= 0)
        pba.SetNextBundleMode(pba::BUNDLE_ONLY_MOTION);
    else
        pba.SetNextBundleMode(pba::BUNDLE_FULL);

    if (this->opts.ba_fixed_intrinsics)
        pba.SetFixedIntrinsics(true);

    pba::ConfigBA* pba_config = pba.GetInternalConfig();
    pba_config->__verbose_cg_iteration = false;
    pba_config->__verbose_level = -2;
    pba_config->__verbose_function_time = false;
    pba_config->__verbose_allocation = false;
    pba_config->__verbose_sse = false;
    //pba_config->__lm_max_iteration = 50;
    //pba_config->__cg_min_iteration = 30;
    //pba_config->__cg_max_iteration = 300;
    pba_config->__lm_delta_threshold = 1e-16;
    pba_config->__lm_mse_threshold = 1e-8;

    /* Prepare camera data. */
    std::vector<pba::CameraT> pba_cams;
    std::vector<int> pba_cams_mapping(this->viewports->size(), -1);
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        if (!this->viewports->at(i).is_added())
            continue;

        CameraPose const& pose = this->viewports->at(i).pose;
        pba::CameraT cam;
        cam.f = pose.get_focal_length()*0.001225;
        std::copy(pose.t.begin(), pose.t.end(), cam.t);
        std::copy(pose.R.begin(), pose.R.end(), cam.m[0]);
        cam.radial = this->viewports->at(i).radial_distortion;
        cam.distortion_type = PBA_DISTORTION_TYPE;
        pba_cams_mapping[i] = pba_cams.size();

        if (single_camera_ba >= 0 && (int)i != single_camera_ba)
            cam.SetConstantCamera();

        pba_cams.push_back(cam);
    }
    std::cout << "Added Cams: "<<pba_cams.size() << "\n";
    pba.SetCameraData(pba_cams.size(), &pba_cams[0]);

    /* Prepare tracks data. */
    std::vector<pba::Point3D> pba_tracks;
    std::vector<int> pba_tracks_mapping(this->tracks->size(), -1);
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        pba::Point3D point;
        std::copy(track.pos.begin(), track.pos.end(), point.xyz);
        pba_tracks_mapping[i] = pba_tracks.size();
        pba_tracks.push_back(point);
    }
    std::cout << "Added PBA track of size: " << pba_tracks.size() << "\n";
    pba.SetPointData(pba_tracks.size(), &pba_tracks[0]);

    /* Prepare feature positions in the images. */
    std::vector<pba::Point2D> pba_2d_points;
    std::vector<int> pba_track_ids;
    std::vector<int> pba_cam_ids;
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        for (std::size_t j = 0; j < track.features.size(); ++j)
        {
            int const view_id = track.features[j].view_id;
            if (!this->viewports->at(view_id).is_added())
                continue;

            int const feature_id = track.features[j].feature_id;
            StereoViewport const& view = this->viewports->at(view_id);
            math::Vec2f const& f2d = view.features.positions[feature_id];
            float const fwidth = static_cast<float>(view.stereo_calibration.image_size.width);
            float const fheight = static_cast<float>(view.stereo_calibration.image_size.height);
            float const fnorm = std::max(fwidth, fheight);

            pba::Point2D point;
            point.x = (f2d[0] + 0.5f - fwidth / 2.0f) / fnorm;//f2d[0];
            point.y = (f2d[1] + 0.5f - fheight / 2.0f) / fnorm;//f2d[1];

            pba_2d_points.push_back(point);
            pba_track_ids.push_back(pba_tracks_mapping[i]);
            pba_cam_ids.push_back(pba_cams_mapping[view_id]);
        }
    }
    pba.SetProjection(pba_2d_points.size(),
        &pba_2d_points[0], &pba_track_ids[0], &pba_cam_ids[0]);

    std::vector<int> focal_mask;
    if (this->opts.ba_shared_intrinsics && single_camera_ba < 0)
    {
        focal_mask.resize(pba_cams.size(), 0);
        pba.SetFocalMask(&focal_mask[0], 1.0f);
    }

    /* Run bundle adjustment. */
    util::WallTimer timer;
    pba.RunBundleAdjustment();

    std::cout << "PBA: MSE " << pba_config->__initial_mse << " -> "
        << pba_config->__final_mse << " ("
        << (char)pba_config->GetBundleReturnCode() << "), "
        << pba_config->__num_lm_success << " iter, "
        << timer.get_elapsed() << "ms." << std::endl;

    /* Transfer camera info and track positions back. */
    std::size_t pba_cam_counter = 0;
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        if (!this->viewports->at(i).is_added())
            continue;

        StereoViewport& view = this->viewports->at(i);
        CameraPose& pose = view.pose;
        pba::CameraT const& cam = pba_cams[pba_cam_counter];

        if (this->opts.verbose_output && single_camera_ba < 0)
        {
            std::cout << "Camera " << i << ", focal length: "
                << pose.get_focal_length() << " -> " << cam.f
                << ", distortion: " << cam.radial << std::endl;
        }

        std::copy(cam.t, cam.t + 3, pose.t.begin());
        std::copy(cam.m[0], cam.m[0] + 9, pose.R.begin());
        //        pose.set_k_matrix(cam.f, 0.0,0.0);

        pose.set_k_matrix(cam.f/0.001225, view.stereo_calibration.leftCamRect->cx(), view.stereo_calibration.leftCamRect->cy());
        view.radial_distortion = cam.radial;
        pba_cam_counter += 1;
    }

    std::size_t pba_track_counter = 0;
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        pba::Point3D const& point = pba_tracks[pba_track_counter];
        std::copy(point.xyz, point.xyz + 3, track.pos.begin());

        pba_track_counter += 1;
    }
}


void
Incremental::bundle_adjustment_intern (int single_camera_ba)
{
#if 0
    /* Configure PBA. */
    pba::SparseBundleCPU pba;
    pba.EnableRadialDistortion(PBA_DISTORTION_TYPE);
    pba.SetNextTimeBudget(0);
    if (single_camera_ba >= 0)
        pba.SetNextBundleMode(pba::BUNDLE_ONLY_MOTION);
    else
        pba.SetNextBundleMode(pba::BUNDLE_FULL);

    if (this->opts.ba_fixed_intrinsics)
        pba.SetFixedIntrinsics(true);

    pba::ConfigBA* pba_config = pba.GetInternalConfig();
    pba_config->__verbose_cg_iteration = false;
    pba_config->__verbose_level = -2;
    pba_config->__verbose_function_time = false;
    pba_config->__verbose_allocation = false;
    pba_config->__verbose_sse = false;
    //pba_config->__lm_max_iteration = 50;
    //pba_config->__cg_min_iteration = 30;
    //pba_config->__cg_max_iteration = 300;
    pba_config->__lm_delta_threshold = 1e-16;
    pba_config->__lm_mse_threshold = 1e-8;

    /* Prepare camera data. */
    std::vector<pba::CameraT> pba_cams;
    std::vector<int> pba_cams_mapping(this->viewports->size(), -1);
#endif
    ceres::Problem problem;

    ceres::LossFunction* lossFunction = new ceres::HuberLoss(1.0);
    std::vector<int> ba_cams_mapping(this->viewports->size(), -1);
    std::vector<std::array<double,6> > veh_poses;

    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        if (!this->viewports->at(i).is_added())
            continue;

        CameraPose const& pose = this->viewports->at(i).pose;
        std::array<double,6> veh_pose;


        get_6dof_pose(pose,veh_pose.begin());
        ba_cams_mapping[i] = veh_poses.size();
        veh_poses.push_back(veh_pose);

    }
    //std::cout << "Added Cams: "<<veh_poses.size() << "\n";
   // pba.SetCameraData(pba_cams.size(), &pba_cams[0]);

    /* Prepare tracks data. */
    std::vector<std::array<double,3> > world_points;
    double auto_color_coeffs[] = {0.350, 0.036, 0.015};
    std::vector<int> ba_tracks_mapping(this->tracks->size(), -1);
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        std::array<double,3> point;
        std::copy(track.pos.begin(), track.pos.end(), point.begin());
        ba_tracks_mapping[i] = world_points.size();
        if(!std::isfinite(point[0]) || !std::isfinite(point[1]) || !std::isfinite(point[2])){
           std::cerr <<"failed in pts track i:" << i << " "<<std::endl;
           throw std::invalid_argument("Point is not finite\n");

        }
        world_points.push_back(point);
    }
    //std::cout << "Added PBA track of size: " << world_points.size() << "\n";

    /* Prepare feature positions in the images. */
    std::vector<pba::Point2D> pba_2d_points;
    std::vector<int> ba_track_ids;
    std::vector<int> ba_cam_ids;
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        for (std::size_t j = 0; j < track.features.size(); ++j)
        {
            int const view_id = track.features[j].view_id;
            if (!this->viewports->at(view_id).is_added())
                continue;

            int const feature_id = track.features[j].feature_id;
            StereoViewport const& view = this->viewports->at(view_id);

            //std::cout << view.features.positions.size() << " "<< view.features_slave.positions.size() <<" "<<view.matches_master_slave.size()<<std::endl;

            math::Vec2f const& f2d = view.features.positions[feature_id];
            math::Vec2f const& f2d_slave = view.features_slave.positions[view.matches_master_slave[feature_id]];
	    math::Vec3uc const& color1 = view.features.colors[feature_id];
	    math::Vec3uc const& color2 = view.features_slave.colors[view.matches_master_slave[feature_id]];

	    math::Vec3uc const& color_slave1 = view.features_slave.colors[feature_id];
	    math::Vec2f const& f2d_slave1 = view.features_slave.positions[feature_id];
	    std::array<double,3> eta;
            std::pair <double, double> uv1, uv2;
	    int color1_r;
	    int color1_g;
	    int color1_b;
	    if (feature_id == view.matches_master_slave[feature_id]) {
	      uv1.first=f2d[0];
	      uv1.second=f2d[1];
	      uv2.first=f2d_slave[0];
	      uv2.second=f2d_slave[1];
	      color1_r = (int)color1[0];
	      color1_g = (int)color1[1];
	      color1_b = (int)color1[2];
	    } else {
	      uv1.first=f2d[0];
	      uv1.second=f2d[1];
	      uv2.first=f2d_slave[0];
	      uv2.second=f2d_slave[1];
	      color1_r = (int)color_slave1[0];
	      color1_g = (int)color_slave1[1];
	      color1_b = (int)color_slave1[2];
	    }


            if(!std::isfinite(uv1.first) || !std::isfinite(uv1.second) || !std::isfinite(uv2.first) ||  !std::isfinite(uv2.second)){
               std::cerr <<"failed in reproj "<<std::endl;
               throw std::invalid_argument("Point is not finite\n");

            }

	    /////////////////////////////////


	    eta[0] = auto_color_coeffs[0];
	    eta[1] = auto_color_coeffs[1];
	    eta[2] = auto_color_coeffs[2];
            if(!std::isfinite(uv1.first) || !std::isfinite(uv1.second) || !std::isfinite(uv2.first) ||  !std::isfinite(uv2.second)){
               std::cerr <<"failed in reproj "<<std::endl;
               throw std::invalid_argument("Point is not finite\n");

            }


	    if(((int)color1[0] != (int)color1[1]) && ((int)color1[1] != (int)color1[2])){
	      //std::cout << "BA: STEREO REPROJECTION AND COLOR****************************" << std::endl;
	      ceres::CostFunction *stereoRepColorError;
	      stereoRepColorError = ceresutil::sfm::SimpleStereoRepColorError::Create(uv1.first, uv1.second,
										     uv2.first, uv2.second,
										     view.stereo_calibration,
										      color1_r, color1_g, color1_b,
										      (int)color2[0], (int)color2[1], (int)color2[2],
										     eta[0], eta[1], eta[2],
										     Eigen::MatrixXd::Identity (2, 2),
										      Eigen::MatrixXd::Identity (2, 2),
										     Eigen::MatrixXd::Identity (1, 1),
										     Eigen::MatrixXd::Identity (1, 1),
										     Eigen::MatrixXd::Identity (1, 1));
	      problem.AddResidualBlock (stereoRepColorError, lossFunction,
					veh_poses[ba_cams_mapping[view_id]].begin(),
					world_points[ba_tracks_mapping[i]].begin(),
				        auto_color_coeffs);
	      problem.SetParameterLowerBound( auto_color_coeffs,0,0);
	      problem.SetParameterLowerBound( auto_color_coeffs,1,0);
	      problem.SetParameterLowerBound( auto_color_coeffs,2,0);
	      problem.SetParameterUpperBound( auto_color_coeffs,0,1);
	      problem.SetParameterUpperBound( auto_color_coeffs,1,1);
	      problem.SetParameterUpperBound( auto_color_coeffs,2,1);
	    } else if (((int)color1[0] == (int)color1[1]) && ((int)color1[1] == (int)color1[2])){
	     
	      // std::cout << "BA: STEREO REPROJECTION ONLY****************************" << std::endl;
            ceres::CostFunction *stereoRepError;
            //std::cout << uv1.first << " "<<uv1.second << " == "<<uv2.first << " "<<uv2.second<<std::endl;

	    /* Each Residual block takes a point and a camera as input and
	     * outputs a 2 dimensional residual. */
	    stereoRepError = ceresutil::sfm::SimpleStereoReprojectionError::Create(uv1.first, uv1.second,
										  uv2.first, uv2.second,
										  view.stereo_calibration,
										  Eigen::MatrixXd::Identity (2, 2),
										  Eigen::MatrixXd::Identity (2, 2));


	    problem.AddResidualBlock (stereoRepError, lossFunction,
				      veh_poses[ba_cams_mapping[view_id]].begin(),
				      world_points[ba_tracks_mapping[i]].begin());
	    }



	    //////////////////////////


        }
    }
   /* pba.SetProjection(pba_2d_points.size(),
        &pba_2d_points[0], &pba_track_ids[0], &pba_cam_ids[0]);

    std::vector<int> focal_mask;
    if (this->opts.ba_shared_intrinsics && single_camera_ba < 0)
    {
        focal_mask.resize(pba_cams.size(), 0);
        pba.SetFocalMask(&focal_mask[0], 1.0f);
    }
*/
    /* Run bundle adjustment. */
    util::WallTimer timer;
    //pba.RunBundleAdjustment();
    ceres::Solver::Options options;
      options.max_num_iterations = 15;
      options.num_threads = 120;
      options.linear_solver_type = ceres::SPARSE_SCHUR;
      options.minimizer_progress_to_stdout = false;
      options.update_state_every_iteration = true;

      ceres::Solver::Summary summary;
      ceres::Solve(options, &problem, &summary);
      if (summary.termination_type == ceres::CONVERGENCE)
        std::cout << summary.BriefReport() << std::endl;
      else{
        if(summary.num_successful_steps < summary.num_unsuccessful_steps)
            std::cout << summary.FullReport() << std::endl;
        else
            std::cout << summary.BriefReport() << " " << summary.num_successful_steps << "/"<<(summary.num_unsuccessful_steps+summary.num_successful_steps)  <<std::endl;

      }
    std::cout << "Ceres took: " /*<< "PBA: MSE " << pba_config->__initial_mse << " -> "
        << pba_config->__final_mse << " ("
        << (char)pba_config->GetBundleReturnCode() << "), "
        << pba_config->__num_lm_success << " iter, "*/
        << timer.get_elapsed_sec() << "s" << std::endl;

    /* Transfer camera info and track positions back. */
    std::size_t pba_cam_counter = 0;
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        if (!this->viewports->at(i).is_added())
            continue;

        StereoViewport& view = this->viewports->at(i);
        CameraPose& pose = view.pose;
        std::array<double,6> const& cam = veh_poses[pba_cam_counter];

       /* if (this->opts.verbose_output && single_camera_ba < 0)
        {
            std::cout << "Camera " << i << ", focal length: "
                << pose.get_focal_length() << " -> " << cam.f
                << ", distortion: " << cam.radial << std::endl;
        }

        math::Vec3d t;
        std::copy(cam.begin(), cam.begin() + 3, t.begin());
        math::Matrix<double, 3, 3> R;

        Eigen::Map<Eigen::Matrix<double,3,3,Eigen::RowMajor> > M(*R);
        Eigen::Matrix3d m;
        m = Eigen::AngleAxisd(cam[3], Eigen::Vector3d::UnitX())
          * Eigen::AngleAxisd(cam[4],  Eigen::Vector3d::UnitY())
          * Eigen::AngleAxisd(cam[5], Eigen::Vector3d::UnitZ());
        M=m;

        math::Matrix4d orig = (R.hstack(t)).vstack(math::Vec4d(0,0,0,1.0));
        math::Matrix4d trans=math::matrix_inverse(orig);
        pose.R=trans.delete_col(3).delete_row(3);
        pose.t = trans.delete_row(3).col(3);*/
        get_cam_pose(cam.begin(),pose);
       // std::cout <<"After t:" << pose.t<< "\n";
       // std::cout <<pose.R<<std::endl<<std::endl;
        //std::copy(cam.m[0], cam.m[0] + 9, pose.R.begin());
        //        pose.set_k_matrix(cam.f, 0.0,0.0);

        //pose.set_k_matrix(cam.f/0.001225, view.stereo_calibration.leftCamRect->cx(), view.stereo_calibration.leftCamRect->cy());
       // view.radial_distortion = cam.radial;
        pba_cam_counter += 1;
    }

    std::size_t pba_track_counter = 0;
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;

        std::array<double,3> const& point = world_points[pba_track_counter];
        std::copy(point.begin(), point.end(), track.pos.begin());

        pba_track_counter += 1;
    }
}
/* ---------------------------------------------------------------- */
inline double alt_error_metric(StereoViewport const& viewport,int feature_id,math::Vec3d const &pos3d){

    CameraPose const& pose = viewport.pose;
    math::Vec2f const& pos2d = viewport.features.positions[feature_id];
    math::Vec2f const& pos2d_slave = viewport.features_slave.positions[viewport.matches_master_slave[feature_id]];
    math::Vec3d pos2d_d(pos2d[0],pos2d[1],pos2d_slave[0]-pos2d[0]);

    ceresutil::sfm::SimpleStereoReprojectionError *stereoRepError;
    //std::cout << uv1.first << " "<<uv1.second << " == "<<uv2.first << " "<<uv2.second<<std::endl;

              /* Each Residual block takes a point and a camera as input and
                  * outputs a 2 dimensional residual. */
              stereoRepError = new ceresutil::sfm::SimpleStereoReprojectionError(pos2d[0], pos2d[1],
                                                                                    pos2d_slave[0], pos2d_slave[1],
                                                                                    viewport.stereo_calibration,
                                                                                    Eigen::MatrixXd::Identity (2, 2),
                                                                                    Eigen::MatrixXd::Identity (2, 2));
    /*double square_error = MATH_POW2(p2d[0] - c.sc2.p2d[0])
        + MATH_POW2(p2d[1] - c.sc2.p2d[1]) +
           MATH_POW2(p2d[2] - (c.sc2.p2d[0]-c.sc2.p2d_slave[0])) ;
*/
    double  res[4];
    double veh_pose[6];
    double world_point[3]={pos3d[0],pos3d[1],pos3d[2]};

    get_6dof_pose(pose,veh_pose);


    stereoRepError->dummyEval(veh_pose,world_point,res);
    math::Vec4d resid(res[0],res[1],res[2],res[3]);
    //std::cout << "Back proj: \n" << world2cam<<std::endl;
 //   math::Vec3d local_point=world2cam.mult(pos3d,1.0);// - cam_poses[i].t;
    //std::cout << "\nPt:\n" << pos3d<<std::endl;
  //  math::Vec3d xyd = cam_pt_to_rect_xyd(pose.K,viewport.stereo_calibration.T_mat(0),local_point);//(x[0] / x[2], x[1] / x[2]);
    double error= resid.square_norm();
    //printf("ALT Error: %f ",error);
//    sfm::bundler::stereo::print_camera_position(pose);
  //  std::cout <<std::setprecision(20)<<pose.t <<std::endl;
    // printf("%e - (%f %f %f)%f %f, %f %f, %f %f\n",error,pose.t[0],pose.t[1],pose.t[2],pos2d_d[0],xyd[0],pos2d_d[1],xyd[1],pos2d_d[2],xyd[2]);

    // printf("%e - (%f %f %f)%f %f, %f %f, %f %f\n",error,pose.t[0],pose.t[1],pose.t[2],pos2d_d[0],xyd[0],pos2d_d[1],xyd[1],pos2d_d[2],xyd[2]);
    delete stereoRepError;
    return error;

}


void
Incremental::invalidate_large_error_tracks (double threshold)
{
    /* Iterate over all tracks and sum reprojection error. */
    std::vector<std::pair<double, std::size_t> > all_errors;
    std::vector<std::pair<double, std::size_t> > alt_all_errors;

    std::vector<double > all_errors_stats;
    //std::vector<double > alt_all_errors_stats;
    std::size_t num_valid_tracks = 0;
   // double alt_total_error=0;
    std::size_t total_num_deleted_track_view=0;

    std::cout << "Doing large error removal threshold: "<<threshold << "\n";
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        if (!this->tracks->at(i).is_valid())
            continue;


        num_valid_tracks += 1;
        math::Vec3f const& pos3d = this->tracks->at(i).pos;
        FeatureReferenceList const& ref = this->tracks->at(i).features;

        if(!std::isfinite(pos3d[0]) || !std::isfinite(pos3d[1]) || !std::isfinite(pos3d[2])){
            this->tracks->at(i).invalidate();
            continue;
        }
        double total_error = 0.0f;
        int num_valid = 0;
        std::size_t num_deleted_track_view=0;

        for (std::size_t j = 0; j < ref.size(); ++j)
        {
            /* Get pose and 2D position of feature. */
            int view_id = ref[j].view_id;
            int feature_id = ref[j].feature_id;

            StereoViewport const& viewport = this->viewports->at(view_id);
            CameraPose const& pose = viewport.pose;
            if (!viewport.is_added() || !pose.is_valid())
                continue;

            double error =calc_square_reproj_error(viewport,feature_id,pos3d);

            if(error > threshold || error < 0){
                this->tracks->at(i).remove_view(view_id);
                num_deleted_track_view += 1;
                total_num_deleted_track_view++;
                //printf("Removing Track %d, view_id %d error: %f\n",i,view_id,error);
                continue;
            }

            total_error +=error;
            //alt_total_error += alt_error_metric(viewport,feature_id,pos3d);
           // printf("Track %d, view_id %ul, error: %f ALT error: %f\n",i,view_id,error,alt_error_metric(viewport,feature_id,pos3d));


            num_valid += 1;
        }
        if( num_valid == 0 ){
            this->tracks->at(i).invalidate();
        }
        //std::cout << "Removed " << num_deleted_track_view << " views from this track\n";
             // fprintf(stdout,"Total Error: %f\n",total_error);

        total_error /= static_cast<double>(num_valid);
       // alt_total_error /= static_cast<double>(num_valid);

       //  fprintf(stdout,"Avg Total Error: %f\n",total_error);
        all_errors.push_back(std::pair<double, int>(total_error, i));
       // alt_all_errors.push_back(std::pair<double, int>(alt_total_error, i));

    }
    std::cout << "Removed " << total_num_deleted_track_view << " views from " << num_valid_tracks<<" tracks\n";

    //fclose(fp);
    if (num_valid_tracks < 2)
        return;
#if 0
    /* Find the 1/2 percentile. */
    std::size_t const nth_position = all_errors.size() / 2;
    std::nth_element(all_errors.begin(),
        all_errors.begin() + nth_position, all_errors.end());
    double const square_threshold = all_errors[nth_position].first
        * this->opts.track_error_threshold_factor;



    /* Find the 1/2 percentile. */
    std::size_t const alt_nth_position = alt_all_errors.size() / 2;
    std::nth_element(alt_all_errors.begin(),
        alt_all_errors.begin() + alt_nth_position, alt_all_errors.end());
    double const alt_square_threshold = alt_all_errors[alt_nth_position].first
        * this->opts.track_error_threshold_factor;


    /* Delete all tracks with errors above the threshold. */
    int num_deleted_tracks = 0;
    printf("%f %f\n",all_errors[0].first , this->opts.new_track_error_threshold);
    for (std::size_t i = nth_position; i < all_errors.size(); ++i)
    {
        if (all_errors[i].first > square_threshold || all_errors[i].first > this->opts.new_track_error_threshold)
        {
            this->tracks->at(all_errors[i].second).invalidate();
            num_deleted_tracks += 1;
        }


    }

    /*for (std::size_t i = 0; i < all_errors.size(); ++i){
        if (all_errors[i].first > this->opts.new_track_error_threshold)
        {
            this->tracks->at(all_errors[i].second).invalidate();
            num_deleted_tracks += 1;
        }


    }*/
    for (std::size_t i = 0; i < all_errors.size(); ++i)
        if(all_errors[i].first <this->opts.new_track_error_threshold)
            all_errors_stats.push_back(all_errors[i].first);

    double sum = std::accumulate(all_errors_stats.begin(), all_errors_stats.end(), 0.0);
    double mean = sum / all_errors_stats.size();

    double alt_sum = std::accumulate(alt_all_errors_stats.begin(), alt_all_errors_stats.end(), 0.0);
    double alt_mean = alt_sum / alt_all_errors_stats.size();

    if (this->opts.verbose_output)
    {
        float percent = 100.0f * static_cast<float>(num_deleted_tracks)
            / static_cast<float>(num_valid_tracks);
        std::cout << "Deleted " << num_deleted_tracks
            << " of " << num_valid_tracks << " tracks ("
            << util::string::get_fixed(percent, 2)
            << "%) above a threshold of "
            << std::sqrt(square_threshold) << "." << std::endl;
        std::cout << "Added "<< all_errors_stats.size() << " valid. With mean error of " << mean <<"."<< std::endl;
        std::cout << "Added "<< alt_all_errors_stats.size() << " valid. With mean error of " << alt_mean <<"."<< std::endl;

    }
#endif
}
template<typename T> void printElement(T t, const int& width)
{
    const char separator    = ' ';

    std::cout << std::left << std::setw(width) << std::setfill(separator) << t;
}

void
Incremental::get_total_error (void)
{
    /* Iterate over all tracks and sum reprojection error. */
    std::vector<int > track_size_data;
    std::vector<int > track_error_data;
    const int numBins = 5;
    std::vector<unsigned long int> bins(numBins,0);
    std::size_t num_valid_tracks = 0;
   // FILE *fp=fopen("invalid.txt","w");
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        if (!this->tracks->at(i).is_valid())
            continue;

        num_valid_tracks += 1;
        math::Vec3f const& pos3d = this->tracks->at(i).pos;
        FeatureReferenceList const& ref = this->tracks->at(i).features;

        double total_error = 0.0f;
        int num_valid = 0;
        for (std::size_t j = 0; j < ref.size(); ++j)
        {
            /* Get pose and 2D position of feature. */
            int view_id = ref[j].view_id;
            int feature_id = ref[j].feature_id;

            StereoViewport const& viewport = this->viewports->at(view_id);
            CameraPose const& pose = viewport.pose;
            if (!viewport.is_added() || !pose.is_valid() || viewport.is_slave())
                continue;
            //fprintf(fp,"%f ",calc_square_reproj_error(viewport,feature_id,pos3d));
            total_error += calc_square_reproj_error(viewport,feature_id,pos3d);
            num_valid += 1;
        }
        if(num_valid <= numBins-1){
            bins[num_valid]++;
        }else
            bins[numBins-1]++;
       // fprintf(stdout,"Total Error: %f\n",total_error);
        track_size_data.push_back(num_valid);
        total_error /= static_cast<double>(num_valid);
        track_error_data.push_back(total_error);
       //  fprintf(stdout,"Avg Total Error: %f\n",total_error);

    }
    //fclose(fp);

    double sumS = std::accumulate(track_size_data.begin(), track_size_data.end(), 0.0);
    double meanS = sumS / track_size_data.size();


    double sum = std::accumulate(track_error_data.begin(), track_error_data.end(), 0.0);
    double mean = sum / track_error_data.size();

    if (this->opts.verbose_output)
    {

        std::cout << "Mean Track Error: " << mean
            << " total error: " << sum << " min: "<< *std::min_element(track_error_data.begin(), track_error_data.end())<<" max: "
            <<*std::max_element(track_error_data.begin(), track_error_data.end())<< std::endl;

        std::cout << "Mean Track Size: " << meanS
            << " min: "<< *std::min_element(track_size_data.begin(), track_size_data.end())<<" max: "
            <<*std::max_element(track_size_data.begin(), track_size_data.end())<< std::endl;
         const int nameWidth     = 6;
         const int numWidth      = 8;
         std::vector<std::string> track_size_hist;
         for(int i=0; i < numBins-1; i++){
             std::stringstream num;
             num<<i;
             track_size_hist.push_back(num.str());
         }
         std::stringstream num;
         num << numBins << "+";

         track_size_hist.push_back(num.str());
         std::cout << "Track Size Histogram" << std::endl;
         for(int i=0; i < (int)track_size_hist.size(); i++){
            printElement(track_size_hist[i],nameWidth);
         }
         std::cout<<std::endl;

         for(int i=0; i < (int)bins.size(); i++){
            printElement(bins[i],nameWidth);
         }
         std::cout<<std::endl;


    }
}

/* ---------------------------------------------------------------- */

void
Incremental::normalize_scene (void)
{
    /* Compute AABB for all camera centers. */
    math::Vec3d aabb_min(std::numeric_limits<double>::max());
    math::Vec3d aabb_max(-std::numeric_limits<double>::max());
    math::Vec3d camera_mean(0.0);
    int num_valid_cameras = 0;
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        CameraPose const& pose = this->viewports->at(i).pose;
        if (!pose.is_valid())
            continue;

        math::Vec3d center = -(pose.R.transposed() * pose.t);
        for (int j = 0; j < 3; ++j)
        {
            aabb_min[j] = std::min(center[j], aabb_min[j]);
            aabb_max[j] = std::max(center[j], aabb_max[j]);
        }
        camera_mean += center;
        num_valid_cameras += 1;
    }

    /* Compute scale and translation. */
    double scale = 10.0 / (aabb_max - aabb_min).maximum();
    math::Vec3d trans = -(camera_mean / static_cast<double>(num_valid_cameras));

    /* Transform every point. */
    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        if (!this->tracks->at(i).is_valid())
            continue;

        this->tracks->at(i).pos = (this->tracks->at(i).pos + trans) * scale;
    }

    /* Transform every camera. */
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {
        CameraPose& pose = this->viewports->at(i).pose;
        if (!pose.is_valid())
            continue;
        pose.t = pose.t * scale - pose.R * trans * scale;
    }
}

void Incremental::add_fixed_stereo_views(void){
    std::map<unsigned int,unsigned int> master_slave_view_id_map;
    TrackList master_slave_tracks;
    std::cout << "Initial Viewport Size " <<  this->viewports->size() << std::endl;
    StereoViewportList slave_views;
    /* Transform every camera. */
    for (std::size_t i = 0; i < this->viewports->size(); ++i)
    {

        StereoViewport &master_view = this->viewports->at(i);
        CameraPose const& pose = master_view.pose;

        //Copy
        StereoViewport slave_view = StereoViewport(this->viewports->at(i));

        slave_view.features=master_view.features_slave;
        slave_view.slave=true;
        CameraPose& slave_pose = slave_view.pose;

        slave_pose.t[0] = slave_pose.t[0]+ master_view.stereo_calibration.T[0];
        //index is idx in this array plus offset as slave views appended to end
        unsigned int slave_view_id=this->viewports->size()+slave_views.size();
        master_slave_view_id_map[i]=slave_view_id;
        slave_views.push_back(slave_view);

    }

    for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        if (!this->tracks->at(i).is_valid())
            continue;

        FeatureReferenceList & ref = this->tracks->at(i).features;
        FeatureReferenceList master_slave_track_refs;
        for (std::size_t j = 0; j < ref.size(); ++j)
        {
            /* Get pose and 2D position of feature. */
            int view_id = ref[j].view_id;
            int feature_id = ref[j].feature_id;

            StereoViewport const& master_view = this->viewports->at(view_id);

            CameraPose const& pose = master_view.pose;
            if (!master_view.is_added() || !pose.is_valid() || !master_slave_view_id_map.count(view_id))
                continue;

             if(master_view.matches_master_slave[feature_id] < 0)
                 continue;

             unsigned int slave_view_id=master_slave_view_id_map[view_id];

             FeatureReference ref_slave(slave_view_id,master_view.matches_master_slave[feature_id]);
             master_slave_track_refs.push_back(ref_slave);
        }
        ref.insert(ref.begin(),master_slave_track_refs.begin(),master_slave_track_refs.end());

    }

        /*for(int j=0; j<(int) master_view.matches_master_slave.size(); j++){
            if(master_view.matches_master_slave[j] < 0)
                continue;
            Track ms_track;
            math::Vec3f const &pos3d = master_view.pts_3d[j];
            ms_track.pos  =  math::matrix_invert_trans(pose.R.hstack(pose.t).vstack(math::Vec4d(0,0,0,1.0))).delete_row(3) * math::Vec4d (pos3d[0],pos3d[1],pos3d[2],1.0);
            FeatureReference ref(i,j);
            FeatureReference ref_slave(slave_view_id,master_view.matches_master_slave[j]);
           // std::cout << "Adding " << ref.view_id << " " <<ref.feature_id << " " <<master_view.features.positions.size()<<"\n";
           // std::cout << "Adding slave : " << ref_slave.view_id << " " <<ref_slave.feature_id <<"\n";

            ms_track.features.push_back(ref);
            ms_track.features.push_back(ref_slave);
            FeatureSet const& features = master_view.features;
            math::Vec3f const feature_color(features.colors[j]);
            math::Vec4f  color = math::Vec4f(feature_color, 1.0f);

            ms_track.color[0] = static_cast<uint8_t>(color[0] / color[3] + 0.5f);
            ms_track.color[1] = static_cast<uint8_t>(color[1] / color[3] + 0.5f);
            ms_track.color[2] = static_cast<uint8_t>(color[2] / color[3] + 0.5f);
            master_slave_tracks.push_back(ms_track);

        }*/

   // std::cout << "Going to add " << slave_views.size() << " and " << master_slave_tracks.size() << " tracks\n";
    this->viewports->insert(this->viewports->end(),slave_views.begin(),slave_views.end());
    //this->tracks->insert(this->tracks->end(),master_slave_tracks.begin(),master_slave_tracks.end());

    /*for (std::size_t i = 0; i < this->tracks->size(); ++i)
    {
        Track const& track = this->tracks->at(i);
        if (!track.is_valid())
            continue;
        for (std::size_t j = 0; j < track.features.size(); ++j)
        {


                for (std::size_t k = 0; k < track.features[j].refs.size(); ++k)
                {
                    int view_id = features[j].refs[k].view_id;
                    if(master_slave_view_id_map.count(view_id) && !slave_track_added[view_id]){
                        master_slave_view_id_map.count(view_id)
                    }
                }
            }
        }*/
}

/* ---------------------------------------------------------------- */

mve::Bundle::Ptr
Incremental::create_bundle (void) const
{
    /* Create bundle data structure. */
    mve::Bundle::Ptr bundle = mve::Bundle::create();
    {
        /* Populate the cameras in the bundle. */
        mve::Bundle::Cameras& bundle_cams = bundle->get_cameras();
        bundle_cams.resize(this->viewports->size());

        for (std::size_t i = 0; i < this->viewports->size(); ++i)
        {

            mve::CameraInfo& cam = bundle_cams[i];
            StereoViewport const& viewport = this->viewports->at(i);
            float const fwidth = static_cast<float>(viewport.stereo_calibration.image_size.width);
            float const fheight = static_cast<float>(viewport.stereo_calibration.image_size.height);
            float const fnorm = std::max(fwidth, fheight);
            CameraPose const& pose = viewport.pose;
            if (!pose.is_valid())
            {
                cam.flen = 0.0f;
                continue;
            }

            cam.flen = static_cast<float>(pose.get_focal_length()) / fnorm;
            cam.ppoint[0] = (pose.K[2])/fwidth;// + 0.5f);
            cam.ppoint[1] = pose.K[5]/fheight;// + 0.5f;
            std::copy(pose.R.begin(), pose.R.end(), cam.rot);
            std::copy(pose.t.begin(), pose.t.end(), cam.trans);
            cam.dist[0] = viewport.radial_distortion
                * MATH_POW2(pose.get_focal_length());
            cam.dist[1] = 0.0f;



        }

        /* Populate the features in the Bundle. */
        mve::Bundle::Features& bundle_feats = bundle->get_features();
        bundle_feats.reserve(this->tracks->size());
        for (std::size_t i = 0; i < this->tracks->size(); ++i)
        {
            Track const& track = this->tracks->at(i);
            if (!track.is_valid())
                continue;

            /* Copy position and color of the track. */
            bundle_feats.push_back(mve::Bundle::Feature3D());
            mve::Bundle::Feature3D& f3d = bundle_feats.back();
            std::copy(track.pos.begin(), track.pos.end(), f3d.pos);
            f3d.color[0] = track.color[0] / 255.0f;
            f3d.color[1] = track.color[1] / 255.0f;
            f3d.color[2] = track.color[2] / 255.0f;
            f3d.refs.reserve(track.features.size());
            for (std::size_t j = 0; j < track.features.size(); ++j)
            {
                /* For each reference copy view ID, feature ID and 2D pos. */
                f3d.refs.push_back(mve::Bundle::Feature2D());
                mve::Bundle::Feature2D& f2d = f3d.refs.back();
                f2d.view_id = track.features[j].view_id;
                f2d.feature_id = track.features[j].feature_id;

                FeatureSet const& features
                    = this->viewports->at(f2d.view_id).features;
              //  std::cout << f2d.view_id << " "<<features.positions.size() << " "<< f2d.feature_id << std::endl;
                math::Vec2f const& f2d_pos
                    = features.positions[f2d.feature_id];
                std::copy(f2d_pos.begin(), f2d_pos.end(), f2d.pos);
            }
        }
    }

    return bundle;
}
STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END
