/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#include "util/timer.h"
#include "mve/image.h"
#include "mve/image_exif.h"
#include "mve/image_tools.h"
#include "sfm/bundler_common.h"
#include "sfm/extract_focal_length.h"
#include "stereo/stereo_features.h"
#include "stereo/plotting_utils.h"
#include <opencv2/highgui/highgui.hpp>
SFM_NAMESPACE_BEGIN
SFM_BUNDLER_NAMESPACE_BEGIN
STEREO_NAMESPACE_BEGIN
std::ostream& operator << (std::ostream &o, const StereoCorrespondence2D3D &a)
  {
  o << "pt3d: " << a.p3d[0] << " " << a.p3d[1] << " "<<a.p3d[2] << std::endl;
  o << "proj: " << a.p2d[0] << " " << a.p2d[1] << std::endl;
  o << "projslave: " << a.p2d_slave[0] << " " << a.p2d_slave[1] << std::endl;

  return o;
  }
std::ostream& operator << (std::ostream &o, const StereoMatchCorrespondence2D3D &a)
  {
  o << "sc1:\n" << a.sc1 << "\nsc2:\n " << a.sc2 << std::endl;
  return o;
  }
void
StereoFeatures::compute (mve::Scene::Ptr scene, StereoViewportList* viewports)
{
    if (scene == nullptr)
        throw std::invalid_argument("Null scene given");
    if (viewports == nullptr)
        throw std::invalid_argument("No viewports given");

    mve::Scene::ViewList const& views = scene->get_views();

    /* Initialize viewports. */
    viewports->clear();
    viewports->resize(views.size());

    std::size_t num_views = viewports->size();
    std::size_t num_done = 0;
    std::size_t total_features=0;

    /* Iterate the scene and compute features. */
#pragma omp parallel for schedule(dynamic,1)
    for (std::size_t i = 0; i < views.size(); ++i)
    {
#pragma omp critical
        {
            num_done += 1;
            float percent = (num_done * 1000 / num_views) / 10.0f;
            std::cout << "\rDetecting features, view " << num_done << " of "
                << num_views << " (" << percent << "%)..." << std::flush;
        }

        if (views[i] == nullptr)
            continue;

        mve::View::Ptr view = views[i];
        mve::ByteImage::Ptr image_master = view->get_byte_image
            (this->opts.image_embedding);
        if (image_master == nullptr)
            continue;

        mve::ByteImage::Ptr image_slave = view->get_byte_image
            (this->opts.image_embedding_slave);
        if (image_slave == nullptr)
            continue;

        /* Rescale image until maximum image size is met. */
        util::WallTimer timer;
        while (this->opts.max_image_size > 0
            && image_master->width() * image_master->height() > this->opts.max_image_size)
            image_master = mve::image::rescale_half_size<uint8_t>(image_master);


        while (this->opts.max_image_size > 0
            && image_slave->width() * image_slave->height() > this->opts.max_image_size)
            image_slave = mve::image::rescale_half_size<uint8_t>(image_slave);

        mve::ByteImage::Ptr calib_data_image = view->get_blob("stereocalib");
        if(calib_data_image->get_byte_size() == 0)
            throw std::invalid_argument("Bad blob stereocalib");
        std::istringstream strm(std::string(calib_data_image->get_byte_pointer(),calib_data_image->get_byte_size()));

        /* Compute features for stereo view. */
        StereoViewport* viewport = &viewports->at(i);
        viewport->stereo_calibration.load_from_file(strm);

        viewport->pose.K=viewport->stereo_calibration.leftCamRect->k_matrix;
        viewport->focal_length=viewport->stereo_calibration.leftCamRect->fx();

        viewport->features.set_options(this->opts.feature_options);
        viewport->features.compute_features(image_master,true,viewport->stereo_calibration);
        std::size_t num_feats_master = viewport->features.positions.size();

        viewport->features_slave.set_options(this->opts.feature_options);
        viewport->features_slave.compute_features(image_slave,false,viewport->stereo_calibration);
        std::size_t num_feats_slave = viewport->features_slave.positions.size();
        /* Computer master slave matches */
        viewport->image=image_master;
        viewport->image_slave=image_slave;
        master_slave_match(*viewport,&viewport->matches_master_slave);
        int valid_matches = triangulate_matches(*viewport,&viewport->matches_master_slave);

        /* Normalize image coordinates. */
        /*float const fwidth = static_cast<float>(viewport->features.width);
        float const fheight = static_cast<float>(viewport->features.height);
        float const fnorm = std::max(fwidth, fheight);
        for (std::size_t j = 0; j < viewport->features.positions.size(); ++j){
            math::Vec2f& pos = viewport->features.positions[j];
            pos[0] = (pos[0] + 0.5f - fwidth / 2.0f) / fnorm;
            pos[1] = (pos[1] + 0.5f - fheight / 2.0f) / fnorm;
        }

        for (std::size_t j = 0; j < viewport->features_slave.positions.size(); ++j){
            math::Vec2f& pos_slave = viewport->features_slave.positions[j];
            pos_slave[0] = (pos_slave[0] + 0.5f - fwidth / 2.0f) / fnorm;
            pos_slave[1] = (pos_slave[1] + 0.5f - fheight / 2.0f) / fnorm;
        }
*/

#pragma omp critical
        {
            std::cout << "\rView ID "
                << util::string::get_filled(view->get_id(), 4, '0') << " ("
                << image_master->width() << "x" << image_master->height() << "), "
                << util::string::get_filled(num_feats_master, 5, ' ') << "," << util::string::get_filled(num_feats_slave, 5, ' ') << " valid:"<< util::string::get_filled(valid_matches, 5, ' ') << " features"
                << ", took " << timer.get_elapsed() << " ms." << std::endl;
            total_features +=viewport->matches_master_slave.size();

        }

        /* Clean up unused embeddings. */
        image_master.reset();
        image_slave.reset();

        view->cache_cleanup();
    }

    std::cout << "\rComputed " << total_features << " features "
        << "for " << num_views << " views (average "
        << (total_features / num_views) << ")." << std::endl;
}


void
StereoFeatures::master_slave_match (StereoViewport &viewport, std::vector<int>* matches_master_slave)
{
    /* Low-res matching if number of features is large. */
    /*if (this->opts.use_lowres_matching
        && view_1.positions.size() * view_2.positions.size() > 1000000)
    {
        int const num_matches = view_1.match_lowres(view_2,
            this->opts.num_lowres_features);
        if (num_matches < this->opts.min_lowres_matches)
        {
            message << "only " << num_matches
                << " of " << this->opts.min_lowres_matches
                << " low-res matches.";
            return;
        }
    }
    */


    /* Perform stereo descriptor matching. */
    sfm::Matching::Result matching_result;

    viewport.features.match(viewport.features_slave,&matching_result,viewport.stereo_calibration,false);
    
    //#pragma omp critical
    //{
    //std::cout << "DISPLAYING\n";
    // cv::Mat outimg;
    //  plotting_utils::drawMatches(outimg,viewport,matching_result,true);
    // cv::namedWindow("A");
    // cv::Mat small;
    //  cv::resize(outimg,small,cv::Size(),0.5,0.5);
    // cv::imshow("A",small);
    // cv::waitKey(2);
    //}
    

    //int num_matches = sfm::Matching::count_consistent_matches(matching_result);
/*
    std::vector<math::Vec2f> positions_matched;
    std::vector<math::Vec3uc> colors_matched;
    Sift::Descriptors sift_descriptors_matched;

    for( int i=0; i <  static_cast<int>(viewport.features.positions.size()); i++){
        if(matching_result.matches_1_2[i] > -1 && matching_result.matches_2_1[matching_result.matches_1_2[i]] == i){
            positions_matched.push_back(viewport.features.positions[i]);
            colors_matched.push_back(viewport.features.colors[i]);
            //sift_descriptors_matched.push_back(viewport.features.sift_descriptors[i]);
        }
    }
    viewport.features.positions.swap(positions_matched);
    viewport.features.colors.swap(colors_matched);
    //viewport.features.sift_descriptors.swap(sift_descriptors_matched);
*/
    /* Create master-slave matching result. */
    matches_master_slave->clear();

    for (int i = 0; i < static_cast<int>(matching_result.matches_1_2.size()); ++i){
        if(matching_result.matches_1_2[i] > -1 && matching_result.matches_2_1[matching_result.matches_1_2[i]] == i){
            matches_master_slave->push_back(matching_result.matches_1_2[i]);
        }else{
            matches_master_slave->push_back(-1);

        }
    }
    //printf("%d %d\n",num_matches,cnt);
}

int
StereoFeatures::triangulate_matches (StereoViewport &viewport, std::vector<int>* matches_master_to_slave)
{
   // FILE *fp=fopen("pts.txt","w");
    //FILE *fp2=fopen("x.txt","w");
    int valid_matches=0;
    for(unsigned int i=0; i < matches_master_to_slave->size(); i++){
        if(matches_master_to_slave->at(i)< 0 ){
            viewport.pts_3d.push_back(math::Vec3d(-999,-999,-999));

        }else{
            math::Vec2f& pos_master=viewport.features.positions[i];
            math::Vec2f& pos_slave=viewport.features_slave.positions[matches_master_to_slave->at(i)];
            math::Vec3d uvd(pos_master[0],pos_master[1],pos_slave[0]-pos_master[0]);
           // std::cout << uvd <<" tx " << viewport.stereo_calibration.T_mat(0)<< " " <<pix_uvd_to_xyz(viewport.stereo_calibration.leftCamRect->k_matrix,viewport.stereo_calibration.T_mat(0),uvd)<< std::endl;
            viewport.pts_3d.push_back(pix_uvd_to_xyz(viewport.stereo_calibration.leftCamRect->k_matrix,viewport.stereo_calibration.T_mat(0),uvd));
             //    std::cout <<"PT: "<<sfm::bundler::stereo::cam_pt_to_rect_xyd( viewport.stereo_calibration.leftCamRect->k_matrix,viewport.stereo_calibration.T_mat(0),
               //                                                        pix_uvd_to_xyz(viewport.stereo_calibration.leftCamRect->k_matrix,viewport.stereo_calibration.T_mat(0),uvd));
            //fprintf(fp,"points->push_back(math::Vec3d(%f,%f,%f));\n",viewport.pts_3d.back()[0],
              //      viewport.pts_3d.back()[1],viewport.pts_3d.back()[2]);
        //      fprintf(fp2,"LC %f RM: %f-- %f\n",pos_master[0],pos_slave[0],viewport.stereo_calibration.T_mat(0));
            valid_matches++;
        }
    }
   // fclose(fp);
   // fclose(fp2);
    return valid_matches;
}
void print_camera_position(sfm::CameraPose pose){
    mve::CameraInfo cam;
    std::copy(pose.R.begin(), pose.R.end(), cam.rot);
    std::copy(pose.t.begin(), pose.t.end(), cam.trans);

    float pos[3];
    cam.fill_camera_pos(pos);
    printf("Camera Pos %.1f %.1f %.1f\n",pos[0],pos[1],pos[2]);
}

STEREO_NAMESPACE_END
SFM_BUNDLER_NAMESPACE_END
SFM_NAMESPACE_END

