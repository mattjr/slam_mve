#pragma once
#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include "stereo/calib.h"
#include "stereo/ssc.hpp"

namespace ceresutil {

template <typename T>
void
copy_from_double_6x1 (const double *v, T* cpy)
{
    cpy[0] = T (v[0]);
    cpy[1] = T (v[1]);
    cpy[2] = T (v[2]);
    cpy[3] = T (v[3]);
    cpy[4] = T (v[4]);
    cpy[5] = T (v[5]);
}

template <typename T>
void
sqrtinf_times_resid_2x2 (const ceres::Matrix &sqrtinf, const T* resid, T* weightedResid)
{
    weightedResid[0] = ((sqrtinf (0,0) * resid[0]) + (sqrtinf (0,1) * resid[1]));
    weightedResid[1] = ((sqrtinf (1,1) * resid[1]));
}

template <typename T>
void
sqrtinf_times_resid_1x1 (const ceres::Matrix &sqrtinf, const T* resid, T* weightedResid)
{
    weightedResid[0] = ((sqrtinf (0,0) * resid[0]));
}


static ceres::Matrix
cov_to_sqrtinf (const ceres::Matrix &covariance)
{
    return covariance.inverse ().llt ().matrixU ();
}

template <typename T>
void
mono_camera_projection (const T* const camera_global,
                        const T* const point_global,
                        const PinholeCameraModel *calib,
                        T* uv)
{
    /* transform point into the camera frame */
    T point_camera[3];
    math::ssc::transform_point_from_i_to_j (camera_global, point_global, point_camera);
   // std::cout << "local " << point_camera[0] << " " << point_camera[1] << " " << point_camera[2] << std::endl;
    /* multiply transformed point by K */
    double focalx = calib->fx();
    double focaly = calib->fy();
    double centerx = calib->cx();
    double centery = calib->cy();

    T predicted_x = ((focalx * point_camera[0]) + (centerx * point_camera[2])) / point_camera[2];
    T predicted_y = ((focaly * point_camera[1]) + (centery * point_camera[2])) / point_camera[2];

    uv[0] = predicted_x;
    uv[1] = predicted_y;
}

template <typename T>
void
stereo_camera_projection (const T* const camera_1_global,
                          const T* const point_global,
                          const Stereo_Calib_CV*calib,
                          T* uv)
{
    /* project into camera_1's image plane */
    mono_camera_projection (camera_1_global, point_global, calib->leftCamRect, uv);

    /* project into camera_2's image plane */
    T camera_2_global[6];
    T x_c2c1[6];
    T x_c1c2[6];
    copy_from_double_6x1 (calib->x_21mu, x_c2c1);
    math::ssc::inverse (x_c2c1, x_c1c2);
    math::ssc::head2tail (camera_1_global, x_c1c2, camera_2_global);
    mono_camera_projection (camera_2_global, point_global, calib->leftCamRect, uv+2);
    //std::cout << *uv << std::endl;
}

/**
 * cost function terms used in offline bundle adjustment
 */
namespace sfm {

struct ReprojectionError {

    static const int RESID_SIZE = 2;
    static const int POSE_BLOCK_SIZE = 6;
    static const int CAM_XFM_BLOCK_SIZE = 6;
    static const int POINT_BLOCK_SIZE = 3;

    /**
     * Compute reprojection error using Ceres auto-differentiation support
     *
     * obs_x : x-coordinate of measured feature
     * obs_y : y-coordinate of measured feature
     * calib : calibration of this camera
     * sqrtinf : 2x2 noise of measured feature.  To convert from a covariance matrix, see cov_to_sqrtinf ()
     */
    ReprojectionError (double obs_x, double obs_y,
                       const PinholeCameraModel &calib,
                       const ceres::Matrix &sqrtinf)
        : obs_x (obs_x), obs_y (obs_y), calib (calib), sqrtinf (sqrtinf) {}

    ~ReprojectionError () {
    }

    /**
     * x_gvi : 6-dof global pose of ith vehicle
     * x_vici : 6-dof transformation from ith vehicle to ith camera
     * point : 3D point location, global
     * residuals : u_observed - u_predicted = u_observed -  K[R t]*point
     */
    template <typename T>
    bool
    operator() (const T* const x_gvi,
                const T* const x_vici,
                const T* const point,
                T *residuals) const {

        /* get camera pose in global frame */
        T camera[6];
        math::ssc::head2tail (x_gvi, x_vici, camera);

        T uv[2];
        T residuals_[2];
        mono_camera_projection (camera, point, &this->calib, uv);
        residuals_[0] = T (obs_x) - uv[0];
        residuals_[1] = T (obs_y) - uv[1];

        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf, residuals_, residuals);

        return true;

    }

    static ceres::CostFunction*
    Create (const double obs_x, const double obs_y,
            const PinholeCameraModel &calib,
            const ceres::Matrix &covariance) {
        ceres::Matrix sqrtinf = cov_to_sqrtinf (covariance);
        return (new ceres::AutoDiffCostFunction<ReprojectionError, RESID_SIZE, POSE_BLOCK_SIZE, CAM_XFM_BLOCK_SIZE, POINT_BLOCK_SIZE> (
                    new ReprojectionError (obs_x, obs_y, calib, sqrtinf)));
    }

    double obs_x;
    double obs_y;
    const PinholeCameraModel calib;
    const ceres::Matrix sqrtinf;
};

struct StereoReprojectionError {

    static const int RESID_SIZE = 4;
    static const int POSE_BLOCK_SIZE = 6;
    static const int SERVO_BLOCK_SIZE = 1;
    static const int SERVO_TO_CAM_BLOCK_SIZE = 6;
    static const int POINT_BLOCK_SIZE = 3;

    /**
     * Compute reprojection error using Ceres auto-differentiation support
     *
     * obs_x_1 : x-coordinate of measured feature in camera_1
     * obs_y_1 : y-coordinate of measured feature in camera_1
     * obs_x_2 : x-coordinate of measured feature in camera_2
     * obs_y_2 : y-coordinate of measured feature in camera_2
     * calib : calibration of stereo rig
     * sqrtinf : 2x2 noise of measured feature.  To convert from a covariance matrix, see cov_to_sqrtinf ()
     */
    StereoReprojectionError (double obs_x_1, double obs_y_1,
                             double obs_x_2, double obs_y_2,
                             const Stereo_Calib_CV &calib,
                             const ceres::Matrix &sqrtinf_1,
                             const ceres::Matrix &sqrtinf_2)
        : obs_x_1 (obs_x_1), obs_y_1 (obs_y_1), obs_x_2 (obs_x_2), obs_y_2 (obs_y_2), 
          calib (calib), sqrtinf_1 (sqrtinf_1), sqrtinf_2 (sqrtinf_2) {
        memset (this->x_vs1, 0, 6*sizeof *(this->x_vs1));
        memset (this->x_vici, 0, 6*sizeof *(this->x_vici));
    }

    ~StereoReprojectionError () {
    }

    /**
     * x_gvi : 6-dof global pose of ith vehicle
     * x_vici : 6-dof transformation from ith vehicle to camera_1 in the ith stereo rig
     * point : 3D point location, global
     * residuals: length-4 vector of measurement errors
     */
    template <typename T>
    bool
    operator() (const T* const x_gvi,
                const T* const servo,
                const T* const x_sci,
                const T* const point,
                T *residuals) const {
        
        /* get pose of camera_1 in vehicle frame */
        T x_vici_[6];
        T x_vs1_[6];
        copy_from_double_6x1 (this->x_vici, x_vici_);
        copy_from_double_6x1 (this->x_vs1, x_vs1_);

        x_vs1_[math::ssc::DOF_P] = servo[0];
        T camera_1[6];

        /* get pose of camera_1 in global frame */
        math::ssc::head2tail (x_vs1_, x_sci, x_vici_);
        math::ssc::head2tail (x_gvi, x_vici_, camera_1);

        T uv[RESID_SIZE];
        T residuals_[RESID_SIZE];
        stereo_camera_projection (camera_1, point, &this->calib, uv);
        residuals_[0] = T (obs_x_1) - uv[0];
        residuals_[1] = T (obs_y_1) - uv[1];
        residuals_[2] = T (obs_x_2) - uv[2];
        residuals_[3] = T (obs_y_2) - uv[3];

        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf_1, residuals_, residuals);
        sqrtinf_times_resid_2x2 (this->sqrtinf_2, residuals_+2, residuals+2);

        return true;

    }

    static ceres::CostFunction*
    Create (const double obs_x_1, const double obs_y_1,
            const double obs_x_2, const double obs_y_2,
            const Stereo_Calib_CV &calib,
            const ceres::Matrix &covariance_1,
            const ceres::Matrix &covariance_2) {
        ceres::Matrix sqrtinf_1 = cov_to_sqrtinf (covariance_1);
        ceres::Matrix sqrtinf_2 = cov_to_sqrtinf (covariance_2);
        return (new ceres::AutoDiffCostFunction<StereoReprojectionError, 
                                                RESID_SIZE, 
                                                POSE_BLOCK_SIZE,
                                                SERVO_BLOCK_SIZE,
                                                SERVO_TO_CAM_BLOCK_SIZE,
                                                POINT_BLOCK_SIZE> (
                    new StereoReprojectionError (obs_x_1, obs_y_1,
                                                 obs_x_2, obs_y_2,
                                                 calib, sqrtinf_1, sqrtinf_2)));
    }

    double obs_x_1;
    double obs_y_1;
    double obs_x_2;
    double obs_y_2;
    const Stereo_Calib_CV calib;
    const ceres::Matrix sqrtinf_1;
    const ceres::Matrix sqrtinf_2;
    double x_vici[6];
    double x_vs1[6];
};

struct RigidStereoReprojectionError {

    static const int RESID_SIZE = 4;
    static const int POSE_BLOCK_SIZE = 6;
    static const int VEH_TO_CAM_BLOCK_SIZE = 6;
    static const int POINT_BLOCK_SIZE = 3;

    /**
     * Compute reprojection error using Ceres auto-differentiation support
     *
     * obs_x_1 : x-coordinate of measured feature in camera_1
     * obs_y_1 : y-coordinate of measured feature in camera_1
     * obs_x_2 : x-coordinate of measured feature in camera_2
     * obs_y_2 : y-coordinate of measured feature in camera_2
     * calib : calibration of stereo rig
     * sqrtinf : 2x2 noise of measured feature.  To convert from a covariance matrix, see cov_to_sqrtinf ()
     */
    RigidStereoReprojectionError (double obs_x_1, double obs_y_1,
                                  double obs_x_2, double obs_y_2,
                                  const Stereo_Calib_CV &calib,
                                  const ceres::Matrix &sqrtinf_1,
                                  const ceres::Matrix &sqrtinf_2)
        : obs_x_1 (obs_x_1), obs_y_1 (obs_y_1), obs_x_2 (obs_x_2), obs_y_2 (obs_y_2), 
          calib (calib), sqrtinf_1 (sqrtinf_1), sqrtinf_2 (sqrtinf_2) {
    }

    ~RigidStereoReprojectionError () {
    }

    /**
     * x_gvi : 6-dof global pose of ith vehicle
     * x_vici : 6-dof transformation from ith vehicle to camera_1 in the ith stereo rig
     * point : 3D point location, global
     * residuals: length-4 vector of measurement errors
     */
    template <typename T>
    bool
    operator() (const T* const x_gvi,
                const T* const x_vc,
                const T* const point,
                T *residuals) const {
        
        /* get pose of camera_1 in global frame */
        T camera_1[6];
        math::ssc::head2tail (x_gvi, x_vc, camera_1);

        T uv[RESID_SIZE];
        T residuals_[RESID_SIZE];
        stereo_camera_projection (camera_1, point, &this->calib, uv);
        residuals_[0] = T (obs_x_1) - uv[0];
        residuals_[1] = T (obs_y_1) - uv[1];
        residuals_[2] = T (obs_x_2) - uv[2];
        residuals_[3] = T (obs_y_2) - uv[3];

        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf_1, residuals_, residuals);
        sqrtinf_times_resid_2x2 (this->sqrtinf_2, residuals_+2, residuals+2);

        return true;

    }

    static ceres::CostFunction*
    Create (const double obs_x_1, const double obs_y_1,
            const double obs_x_2, const double obs_y_2,
            const Stereo_Calib_CV &calib,
            const ceres::Matrix &covariance_1,
            const ceres::Matrix &covariance_2) {
        ceres::Matrix sqrtinf_1 = cov_to_sqrtinf (covariance_1);
        ceres::Matrix sqrtinf_2 = cov_to_sqrtinf (covariance_2);
        return (new ceres::AutoDiffCostFunction<RigidStereoReprojectionError, 
                                                RESID_SIZE, 
                                                POSE_BLOCK_SIZE,
                                                VEH_TO_CAM_BLOCK_SIZE,
                                                POINT_BLOCK_SIZE> (
                    new RigidStereoReprojectionError (obs_x_1, obs_y_1,
                                                      obs_x_2, obs_y_2,
                                                      calib, sqrtinf_1, sqrtinf_2)));
    }

    double obs_x_1;
    double obs_y_1;
    double obs_x_2;
    double obs_y_2;
    const Stereo_Calib_CV calib;
    const ceres::Matrix sqrtinf_1;
    const ceres::Matrix sqrtinf_2;
};


struct SimpleStereoReprojectionError {

    static const int RESID_SIZE = 4;
    static const int POSE_BLOCK_SIZE = 6;
    static const int POINT_BLOCK_SIZE = 3;

    /**
     * Compute reprojection error using Ceres auto-differentiation support
     *
     * obs_x_1 : x-coordinate of measured feature in camera_1
     * obs_y_1 : y-coordinate of measured feature in camera_1
     * obs_x_2 : x-coordinate of measured feature in camera_2
     * obs_y_2 : y-coordinate of measured feature in camera_2
     * calib : calibration of stereo rig
     * sqrtinf : 2x2 noise of measured feature.  To convert from a covariance matrix, see cov_to_sqrtinf ()
     */
    SimpleStereoReprojectionError (double obs_x_1, double obs_y_1,
                                  double obs_x_2, double obs_y_2,
                                  const Stereo_Calib_CV &calib,
                                  const ceres::Matrix &sqrtinf_1,
                                  const ceres::Matrix &sqrtinf_2)
        : obs_x_1 (obs_x_1), obs_y_1 (obs_y_1), obs_x_2 (obs_x_2), obs_y_2 (obs_y_2),
          calib (calib), sqrtinf_1 (sqrtinf_1), sqrtinf_2 (sqrtinf_2) {
    }

    ~SimpleStereoReprojectionError () {
    }

    /**
     * x_gci : 6-dof global pose of ith camera
     * point : 3D point location, global
     * residuals: length-4 vector of measurement errors
     */
    template <typename T>
    bool
    operator() (const T* const x_gci,
                const T* const point,
                T *residuals) const {

        /* get pose of camera_1 in global frame */
        T camera_1[6];
        camera_1[0]= x_gci[0];
        camera_1[1]= x_gci[1];
        camera_1[2]= x_gci[2];
        camera_1[3]= x_gci[3];
        camera_1[4]= x_gci[4];
        camera_1[5]= x_gci[5];


        T uv[RESID_SIZE];
        T residuals_[RESID_SIZE];
        stereo_camera_projection (camera_1, point, &this->calib, uv);
        residuals_[0] = T (obs_x_1) - uv[0];
        residuals_[1] = T (obs_y_1) - uv[1];
        residuals_[2] = T (obs_x_2) - uv[2];
        residuals_[3] = T (obs_y_2) - uv[3];
     /*   std::cout <<"camera 1 "<<    camera_1[0] << " "<<    camera_1[1] << " " <<   camera_1[2]<<std::endl;
        std::cout <<"1u predicted " << uv[0] << "obs " << obs_x_1<<std::endl;
        std::cout <<"1v predicted " << uv[1] << "obs " << obs_y_1<<std::endl;
        std::cout <<"2u predicted " << uv[2] << "obs " << obs_x_2<<std::endl;
        std::cout <<"2v predicted " << uv[3] << "obs " << obs_y_2<<std::endl;
*/
        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf_1, residuals_, residuals);
        sqrtinf_times_resid_2x2 (this->sqrtinf_2, residuals_+2, residuals+2);

        return true;

    }

    /**
     * x_gci : 6-dof global pose of ith camera
     * point : 3D point location, global
     * residuals: length-4 vector of measurement errors
     */
    bool
    dummyEval(const double* const x_gci,
                const double* const point,
                double *residuals) const {

        /* get pose of camera_1 in global frame */
        double camera_1[6];
        camera_1[0]= x_gci[0];
        camera_1[1]= x_gci[1];
        camera_1[2]= x_gci[2];
        camera_1[3]= x_gci[3];
        camera_1[4]= x_gci[4];
        camera_1[5]= x_gci[5];


        double uv[RESID_SIZE];
        double residuals_[RESID_SIZE];
        stereo_camera_projection (camera_1, point, &this->calib, uv);
        residuals_[0] = double (obs_x_1) - uv[0];
        residuals_[1] = double (obs_y_1) - uv[1];
        residuals_[2] = double (obs_x_2) - uv[2];
        residuals_[3] = double (obs_y_2) - uv[3];
        /*std::cout <<"camera 1 "<<    camera_1[0] << " "<<    camera_1[1] << " " <<   camera_1[2]<<" " <<   camera_1[3]<<" " <<   camera_1[4]<<" " <<   camera_1[5]<<std::endl;
        std::cout <<"1u predicted " << uv[0] << "obs " << obs_x_1<<std::endl;
        std::cout <<"1v predicted " << uv[1] << "obs " << obs_y_1<<std::endl;
        std::cout <<"2u predicted " << uv[2] << "obs " << obs_x_2<<std::endl;
        std::cout <<"2v predicted " << uv[3] << "obs " << obs_y_2<<std::endl;
*/
        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf_1, residuals_, residuals);
        sqrtinf_times_resid_2x2 (this->sqrtinf_2, residuals_+2, residuals+2);

        return true;

    }

    static ceres::CostFunction*
    Create (const double obs_x_1, const double obs_y_1,
            const double obs_x_2, const double obs_y_2,
            const Stereo_Calib_CV &calib,
            const ceres::Matrix &covariance_1,
            const ceres::Matrix &covariance_2) {
        ceres::Matrix sqrtinf_1 = cov_to_sqrtinf (covariance_1);
        ceres::Matrix sqrtinf_2 = cov_to_sqrtinf (covariance_2);
        return (new ceres::AutoDiffCostFunction<SimpleStereoReprojectionError,
                                                RESID_SIZE,
                                                POSE_BLOCK_SIZE,
                                                POINT_BLOCK_SIZE> (
                    new SimpleStereoReprojectionError (obs_x_1, obs_y_1,
                                                      obs_x_2, obs_y_2,
                                                      calib, sqrtinf_1, sqrtinf_2)));
    }

    double obs_x_1;
    double obs_y_1;
    double obs_x_2;
    double obs_y_2;
    const Stereo_Calib_CV calib;
    const ceres::Matrix sqrtinf_1;
    const ceres::Matrix sqrtinf_2;
};

} /* namespace sfm */


struct ReprojectionErrorFixedCamera {

    static const int RESID_SIZE = 2;
    static const int POINT_BLOCK_SIZE = 3;

    /**
     * Compute reprojection error using Ceres auto-differentiation support
     *
     * obs_x : x-coordinate of measured feature
     * obs_y : y-coordinate of measured feature
     * calib : calibration of this camera
     * sqrtinf : 2x2 noise of measured feature.  To convert from a covariance matrix, see cov_to_sqrtinf ()
     * camera : *Fixed* 6x1 pose of camera (not subject to optimization)
     */
    ReprojectionErrorFixedCamera (double obs_x, double obs_y,
                                  const PinholeCameraModel &calib,
                                  const ceres::Matrix &sqrtinf,
                                  const double* camera)
        : obs_x (obs_x), obs_y (obs_y), calib (calib), sqrtinf (sqrtinf) {
        memcpy (this->camera, camera, 6*sizeof *camera);
    }

    ~ReprojectionErrorFixedCamera () {
    }

    /**
     * point : 3D point location, global
     * residuals : u_observed - u_predicted = u_observed -  K[R t]*point
     */
    template <typename T>
    bool
    operator() (const T* const point,
                T *residuals) const {

        T camera_[6];
        copy_from_double_6x1 (this->camera, camera_);

        T uv[2];
        T residuals_[2];
        mono_camera_projection (camera_, point, &this->calib, uv);
        residuals_[0] = T (obs_x) - uv[0];
        residuals_[1] = T (obs_y) - uv[1];

        /* weight by square root infromation matrix */
        sqrtinf_times_resid_2x2 (this->sqrtinf, residuals_, residuals);

        return true;

    }

    static ceres::CostFunction*
    Create (const double obs_x, const double obs_y,
            const PinholeCameraModel &calib,
            const ceres::Matrix &covariance,
            const double *camera) {
        ceres::Matrix sqrtinf = cov_to_sqrtinf (covariance);
        return (new ceres::AutoDiffCostFunction<ReprojectionErrorFixedCamera, RESID_SIZE, POINT_BLOCK_SIZE> (
                    new ReprojectionErrorFixedCamera (obs_x, obs_y, calib, sqrtinf, camera)));
    }

    double obs_x;
    double obs_y;
    const PinholeCameraModel calib;
    const ceres::Matrix sqrtinf;
    double camera[6];
};


} /* namespace ceresutil */


